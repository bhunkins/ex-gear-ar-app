﻿using UnityEngine;
using Vuforia;

public class PlaySoundOnTrack : MonoBehaviour, ITrackableEventHandler
{
	public GameObject imageTarget; 
	private AudioSource audio;
	private TrackableBehaviour mTrackableBehaviour;

	void Start()
	{
		// register as trackable
		mTrackableBehaviour = imageTarget.GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
		
		// init audio parameters
		audio = GetComponent<AudioSource>();
		audio.playOnAwake = false;
	}

	public void OnTrackableStateChanged( TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		// return if not tracked
		if (newStatus != TrackableBehaviour.Status.TRACKED &&
		    newStatus != TrackableBehaviour.Status.EXTENDED_TRACKED) return;
		
		// return if audio already playing
		if (audio.isPlaying) return;
		audio.Play();
	}   
}


