﻿///*****************************************************************
// * starts this animation when Vuforia Track found
// ****************************************************************/
//
using Vuforia;
using UnityEngine;

public class AnimateOnTrack : MonoBehaviour, ITrackableEventHandler
{
	public GameObject trackableObject;
	private TrackableBehaviour mTrackableBehaviour;
	private Animator anim;

	void Start ()
	{
		mTrackableBehaviour = trackableObject.GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}

		gameObject.SetActive(false);
		anim = GetComponent<Animator>();
	}

	public void OnTrackableStateChanged (
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			gameObject.SetActive(true);
			anim.SetFloat("direction", 1f);
		}
		else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
		         newStatus == TrackableBehaviour.Status.NO_POSE)
		{
			gameObject.SetActive(false);
			anim.SetFloat("direction", 0);
		}
	}
}
