﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

[RequireComponent(typeof(AudioSource))]
public class PlayAudioClips : MonoBehaviour, ITrackableEventHandler
{
    public List<AudioClip> audioClips;
    
    private AudioSource audiosource;
    private int currClip;
    private TrackableBehaviour mTrackableBehaviour;
    
    void Start()
    {
        // register as trackable
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        
        
        currClip = 0;
        audiosource = GetComponent<AudioSource>();
    }
    
    public void OnTrackableStateChanged( TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        // return if not tracked
        if (newStatus != TrackableBehaviour.Status.TRACKED &&
            newStatus != TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
//            audiosource.Stop();
            return;
        }
        if (!audiosource.isPlaying)
        {
            PlaySound();
        }
    }   

    void PlaySound ()
    {
        audiosource.clip = audioClips[currClip];
        audiosource.Play();
        StartCoroutine(WaitForSound(audioClips[currClip]));
    }
    
    IEnumerator WaitForSound(AudioClip Sound)
    {
        yield return new WaitUntil(() => audiosource.isPlaying == false);
        currClip += 1;
        if (currClip == audioClips.Count) currClip = 0;
        PlaySound();
        
        Debug.Log("Current Audio Clip: "  + currClip);
    }
}
