﻿using UnityEngine;
using Vuforia;

public class StartParticlesOnTracking : MonoBehaviour, ITrackableEventHandler
{
	public GameObject trackableObject;
	private TrackableBehaviour mTrackableBehaviour;
	private ParticleSystem particles;

	void Start () {
		mTrackableBehaviour = trackableObject.GetComponent<TrackableBehaviour>();
		particles = GetComponent<ParticleSystem>();
		
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
		
		particles.Stop();
	}
	
	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			particles.Play();
		}
		else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
		         newStatus == TrackableBehaviour.Status.NO_POSE)
		{
			particles.Stop();
		}
	}
}
