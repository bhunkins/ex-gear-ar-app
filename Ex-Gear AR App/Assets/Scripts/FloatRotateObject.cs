﻿using UnityEngine;
using System;
using System.Collections;

public class FloatRotateObject : MonoBehaviour
{
	float originalY;

	public bool rotateEnabled = true;
	public short rotateSpeedX = 1;
	public short rotateSpeedY = 1;
	public short rotateSpeedZ = 1;

	public bool floatEnabled = true;
	public float floatStrength = 1;


	void Start()
	{
		this.originalY = this.transform.position.y;
	}

	void Update()
	{
		// FLOAT
		if (floatEnabled) {
			transform.position = new Vector3(transform.position.x,
				originalY + ((float)Math.Sin(Time.time) * floatStrength),
				transform.position.z);
		}
			
		// ROTATE
		if (rotateEnabled) {
			float rotateValueX = Time.deltaTime * rotateSpeedX;
			float rotateValueY = Time.deltaTime * rotateSpeedY;
			float rotateValueZ = Time.deltaTime * rotateSpeedZ;
			transform.Rotate(rotateValueX, rotateValueY, rotateValueZ);
		}

	}
}