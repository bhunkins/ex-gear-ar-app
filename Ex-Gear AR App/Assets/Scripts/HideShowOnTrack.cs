﻿///* ATTACH TO IMAGE TARGET AND ASSIGN GAME OBJECTS
// * THAT ARE TO SHOW OR HIDE WHEN TRACKING DETECTED.
// * USING THE toggleStates BOOL WILL INVERT THE VIEW
// * STATES WHEN TRACKING IS LOST
// ***************************************************/


using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class HideShowOnTrack : MonoBehaviour, ITrackableEventHandler {
	[SerializeField]
	List<GameObject> ShowObectsWhenTracked;
	
	[SerializeField]
	List<GameObject> HideObectsWhenTracked;

	[SerializeField]
	bool toggleStates = false;
	
	private TrackableBehaviour mTrackableBehaviour;
		
	
	void Start () {
		mTrackableBehaviour = this.GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		
	}

	public void OnTrackableStateChanged (
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			// tracking found
			ShowObjects(true);
			HideObjects(false);

		} else {
			// tracking lost
			if (!toggleStates) return;
			ShowObjects(false);
			HideObjects(true);
		}
	}

	private void ShowObjects (bool state)
	{
		foreach (GameObject obj in ShowObectsWhenTracked)
			obj.SetActive (state);
		
	}
	
	private void HideObjects (bool state)
	{
		foreach (GameObject obj in HideObectsWhenTracked)
			obj.SetActive(state);
	}
}
