﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5;
// DecalDestroyer
struct DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E;
// ExtinguishableFire
struct ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD;
// GunAim
struct GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880;
// Lighting
struct Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37;
// ParticleExamples[]
struct ParticleExamplesU5BU5D_t1288EC92287C2C169E23AECAF8E5532A7D003FA6;
// PlayAudioClips
struct PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1;
// Readme/Section[]
struct SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t758C8FD2F54C417732AA0EFD9E5CB03E9F88FEC5;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>>
struct Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>>
struct Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9;
// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker>
struct Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t173E251C9CF6D9E02C6BF71D05C0DD07220A28A9;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent>
struct List_1_t2762C811E470D336E31761384C6E5382164DA4C7;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_tE1500C473509EDCE2ED838547DF41114B3785932;
// System.Func`2<System.Type,Vuforia.Tracker>
struct Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580;
// System.Func`2<Vuforia.Tracker,System.Boolean>
struct Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608;
// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor>
struct Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338;
// Vuforia.DataSet
struct DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644;
// Vuforia.ITrackerManager
struct ITrackerManager_tF9B9BD66F3A783EE6406F6E14B874385FE5E156B;
// Vuforia.ImageTarget
struct ImageTarget_t916FA6EEDF77BFCE0C8490D8E534D871354B7E1C;
// Vuforia.ObjectTracker
struct ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E;
// Vuforia.StateManager
struct StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4;
// Vuforia.VirtualButton
struct VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654;
// Vuforia.VuforiaARController
struct VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2;
// Vuforia.WebCam
struct WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2;
// WFX_BulletHoleDecal
struct WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5;
// WFX_Demo
struct WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E;
// WFX_Demo_New
struct WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB;
// WFX_LightFlicker
struct WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E;




#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#define U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tBB5D6FDE6CBE0067E5DC774822D5054F01745F8C 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#ifndef U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#define U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TCE4B768174CDE0294B05DD8ED59A7763FF34E99B_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_H
#ifndef U3CCHECKIFALIVEU3ED__2_TF5EA91DDEAE9EB6B7FAF0D313578E96C08450542_H
#define U3CCHECKIFALIVEU3ED__2_TF5EA91DDEAE9EB6B7FAF0D313578E96C08450542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutoDestructShuriken/<CheckIfAlive>d__2
struct  U3CCheckIfAliveU3Ed__2_tF5EA91DDEAE9EB6B7FAF0D313578E96C08450542  : public RuntimeObject
{
public:
	// System.Int32 CFX_AutoDestructShuriken/<CheckIfAlive>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CFX_AutoDestructShuriken CFX_AutoDestructShuriken/<CheckIfAlive>d__2::<>4__this
	CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ed__2_tF5EA91DDEAE9EB6B7FAF0D313578E96C08450542, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ed__2_tF5EA91DDEAE9EB6B7FAF0D313578E96C08450542, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ed__2_tF5EA91DDEAE9EB6B7FAF0D313578E96C08450542, ___U3CU3E4__this_2)); }
	inline CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKIFALIVEU3ED__2_TF5EA91DDEAE9EB6B7FAF0D313578E96C08450542_H
#ifndef U3CSTARTU3ED__1_T18E1B08206054478B6F37BB254E8005B78622625_H
#define U3CSTARTU3ED__1_T18E1B08206054478B6F37BB254E8005B78622625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecalDestroyer/<Start>d__1
struct  U3CStartU3Ed__1_t18E1B08206054478B6F37BB254E8005B78622625  : public RuntimeObject
{
public:
	// System.Int32 DecalDestroyer/<Start>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DecalDestroyer/<Start>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DecalDestroyer DecalDestroyer/<Start>d__1::<>4__this
	DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_t18E1B08206054478B6F37BB254E8005B78622625, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_t18E1B08206054478B6F37BB254E8005B78622625, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__1_t18E1B08206054478B6F37BB254E8005B78622625, ___U3CU3E4__this_2)); }
	inline DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__1_T18E1B08206054478B6F37BB254E8005B78622625_H
#ifndef U3CEXTINGUISHINGU3ED__6_T1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191_H
#define U3CEXTINGUISHINGU3ED__6_T1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtinguishableFire/<Extinguishing>d__6
struct  U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191  : public RuntimeObject
{
public:
	// System.Int32 ExtinguishableFire/<Extinguishing>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ExtinguishableFire/<Extinguishing>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ExtinguishableFire ExtinguishableFire/<Extinguishing>d__6::<>4__this
	ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD * ___U3CU3E4__this_2;
	// System.Single ExtinguishableFire/<Extinguishing>d__6::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191, ___U3CU3E4__this_2)); }
	inline ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191, ___U3CelapsedTimeU3E5__2_3)); }
	inline float get_U3CelapsedTimeU3E5__2_3() const { return ___U3CelapsedTimeU3E5__2_3; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_3() { return &___U3CelapsedTimeU3E5__2_3; }
	inline void set_U3CelapsedTimeU3E5__2_3(float value)
	{
		___U3CelapsedTimeU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXTINGUISHINGU3ED__6_T1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191_H
#ifndef U3CSTARTINGFIREU3ED__7_TBFAD7A923C81995633CAE044A53DC33229EF873F_H
#define U3CSTARTINGFIREU3ED__7_TBFAD7A923C81995633CAE044A53DC33229EF873F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtinguishableFire/<StartingFire>d__7
struct  U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F  : public RuntimeObject
{
public:
	// System.Int32 ExtinguishableFire/<StartingFire>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ExtinguishableFire/<StartingFire>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ExtinguishableFire ExtinguishableFire/<StartingFire>d__7::<>4__this
	ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD * ___U3CU3E4__this_2;
	// System.Single ExtinguishableFire/<StartingFire>d__7::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F, ___U3CU3E4__this_2)); }
	inline ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F, ___U3CelapsedTimeU3E5__2_3)); }
	inline float get_U3CelapsedTimeU3E5__2_3() const { return ___U3CelapsedTimeU3E5__2_3; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_3() { return &___U3CelapsedTimeU3E5__2_3; }
	inline void set_U3CelapsedTimeU3E5__2_3(float value)
	{
		___U3CelapsedTimeU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTINGFIREU3ED__7_TBFAD7A923C81995633CAE044A53DC33229EF873F_H
#ifndef U3CFLASHU3ED__17_T8698C2DE91EE13B4795055166170A1AB56F976BC_H
#define U3CFLASHU3ED__17_T8698C2DE91EE13B4795055166170A1AB56F976BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lighting/<flash>d__17
struct  U3CflashU3Ed__17_t8698C2DE91EE13B4795055166170A1AB56F976BC  : public RuntimeObject
{
public:
	// System.Int32 Lighting/<flash>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Lighting/<flash>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Lighting Lighting/<flash>d__17::<>4__this
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CflashU3Ed__17_t8698C2DE91EE13B4795055166170A1AB56F976BC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CflashU3Ed__17_t8698C2DE91EE13B4795055166170A1AB56F976BC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CflashU3Ed__17_t8698C2DE91EE13B4795055166170A1AB56F976BC, ___U3CU3E4__this_2)); }
	inline Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLASHU3ED__17_T8698C2DE91EE13B4795055166170A1AB56F976BC_H
#ifndef U3CKEEPONU3ED__19_TBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA_H
#define U3CKEEPONU3ED__19_TBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lighting/<keepOn>d__19
struct  U3CkeepOnU3Ed__19_tBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA  : public RuntimeObject
{
public:
	// System.Int32 Lighting/<keepOn>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Lighting/<keepOn>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Lighting Lighting/<keepOn>d__19::<>4__this
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CkeepOnU3Ed__19_tBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CkeepOnU3Ed__19_tBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CkeepOnU3Ed__19_tBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA, ___U3CU3E4__this_2)); }
	inline Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CKEEPONU3ED__19_TBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA_H
#ifndef U3CSETFLASHINGOFFU3ED__20_TAE42F6A48A1229ACEC0C900F99D937D4F1793577_H
#define U3CSETFLASHINGOFFU3ED__20_TAE42F6A48A1229ACEC0C900F99D937D4F1793577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lighting/<setFlashingOff>d__20
struct  U3CsetFlashingOffU3Ed__20_tAE42F6A48A1229ACEC0C900F99D937D4F1793577  : public RuntimeObject
{
public:
	// System.Int32 Lighting/<setFlashingOff>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Lighting/<setFlashingOff>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Lighting Lighting/<setFlashingOff>d__20::<>4__this
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CsetFlashingOffU3Ed__20_tAE42F6A48A1229ACEC0C900F99D937D4F1793577, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CsetFlashingOffU3Ed__20_tAE42F6A48A1229ACEC0C900F99D937D4F1793577, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CsetFlashingOffU3Ed__20_tAE42F6A48A1229ACEC0C900F99D937D4F1793577, ___U3CU3E4__this_2)); }
	inline Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETFLASHINGOFFU3ED__20_TAE42F6A48A1229ACEC0C900F99D937D4F1793577_H
#ifndef U3CSETREVU3ED__18_TCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C_H
#define U3CSETREVU3ED__18_TCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lighting/<setRev>d__18
struct  U3CsetRevU3Ed__18_tCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C  : public RuntimeObject
{
public:
	// System.Int32 Lighting/<setRev>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Lighting/<setRev>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Lighting Lighting/<setRev>d__18::<>4__this
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CsetRevU3Ed__18_tCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CsetRevU3Ed__18_tCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CsetRevU3Ed__18_tCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C, ___U3CU3E4__this_2)); }
	inline Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETREVU3ED__18_TCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C_H
#ifndef U3CWAITFORSOUNDU3ED__7_T5273E7DF11061419F414D0B1CBECDC04702B258A_H
#define U3CWAITFORSOUNDU3ED__7_T5273E7DF11061419F414D0B1CBECDC04702B258A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayAudioClips/<WaitForSound>d__7
struct  U3CWaitForSoundU3Ed__7_t5273E7DF11061419F414D0B1CBECDC04702B258A  : public RuntimeObject
{
public:
	// System.Int32 PlayAudioClips/<WaitForSound>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PlayAudioClips/<WaitForSound>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PlayAudioClips PlayAudioClips/<WaitForSound>d__7::<>4__this
	PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForSoundU3Ed__7_t5273E7DF11061419F414D0B1CBECDC04702B258A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForSoundU3Ed__7_t5273E7DF11061419F414D0B1CBECDC04702B258A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForSoundU3Ed__7_t5273E7DF11061419F414D0B1CBECDC04702B258A, ___U3CU3E4__this_2)); }
	inline PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORSOUNDU3ED__7_T5273E7DF11061419F414D0B1CBECDC04702B258A_H
#ifndef SECTION_TDCE8FED365051651AC2BFB27F2230DDC5603C985_H
#define SECTION_TDCE8FED365051651AC2BFB27F2230DDC5603C985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Readme/Section
struct  Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985  : public RuntimeObject
{
public:
	// System.String Readme/Section::heading
	String_t* ___heading_0;
	// System.String Readme/Section::text
	String_t* ___text_1;
	// System.String Readme/Section::linkText
	String_t* ___linkText_2;
	// System.String Readme/Section::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_heading_0() { return static_cast<int32_t>(offsetof(Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985, ___heading_0)); }
	inline String_t* get_heading_0() const { return ___heading_0; }
	inline String_t** get_address_of_heading_0() { return &___heading_0; }
	inline void set_heading_0(String_t* value)
	{
		___heading_0 = value;
		Il2CppCodeGenWriteBarrier((&___heading_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_linkText_2() { return static_cast<int32_t>(offsetof(Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985, ___linkText_2)); }
	inline String_t* get_linkText_2() const { return ___linkText_2; }
	inline String_t** get_address_of_linkText_2() { return &___linkText_2; }
	inline void set_linkText_2(String_t* value)
	{
		___linkText_2 = value;
		Il2CppCodeGenWriteBarrier((&___linkText_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTION_TDCE8FED365051651AC2BFB27F2230DDC5603C985_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#define ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293  : public RuntimeObject
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293, ___mVuforiaBehaviour_0)); }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#ifndef EYEWEARCALIBRATIONPROFILEMANAGER_TDC16C656B1E63F9F7FA7A165262CF001CFBF672A_H
#define EYEWEARCALIBRATIONPROFILEMANAGER_TDC16C656B1E63F9F7FA7A165262CF001CFBF672A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearCalibrationProfileManager
struct  EyewearCalibrationProfileManager_tDC16C656B1E63F9F7FA7A165262CF001CFBF672A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARCALIBRATIONPROFILEMANAGER_TDC16C656B1E63F9F7FA7A165262CF001CFBF672A_H
#ifndef EYEWEARUSERCALIBRATOR_TFA27D2BE8A64087E551C93ADE2489D69A0A79A49_H
#define EYEWEARUSERCALIBRATOR_TFA27D2BE8A64087E551C93ADE2489D69A0A79A49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearUserCalibrator
struct  EyewearUserCalibrator_tFA27D2BE8A64087E551C93ADE2489D69A0A79A49  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARUSERCALIBRATOR_TFA27D2BE8A64087E551C93ADE2489D69A0A79A49_H
#ifndef TRACKERMANAGER_T4E97F9E410AEF74F864668111D4E1AE7B8EA7530_H
#define TRACKERMANAGER_T4E97F9E410AEF74F864668111D4E1AE7B8EA7530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager
struct  TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530  : public RuntimeObject
{
public:
	// Vuforia.StateManager Vuforia.TrackerManager::mStateManager
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1 * ___mStateManager_1;
	// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager::mTrackers
	Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03 * ___mTrackers_2;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.Type,Vuforia.Tracker>> Vuforia.TrackerManager::mTrackerCreators
	Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775 * ___mTrackerCreators_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<Vuforia.Tracker,System.Boolean>> Vuforia.TrackerManager::mTrackerNativeDeinitializers
	Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9 * ___mTrackerNativeDeinitializers_4;

public:
	inline static int32_t get_offset_of_mStateManager_1() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530, ___mStateManager_1)); }
	inline StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1 * get_mStateManager_1() const { return ___mStateManager_1; }
	inline StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1 ** get_address_of_mStateManager_1() { return &___mStateManager_1; }
	inline void set_mStateManager_1(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1 * value)
	{
		___mStateManager_1 = value;
		Il2CppCodeGenWriteBarrier((&___mStateManager_1), value);
	}

	inline static int32_t get_offset_of_mTrackers_2() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530, ___mTrackers_2)); }
	inline Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03 * get_mTrackers_2() const { return ___mTrackers_2; }
	inline Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03 ** get_address_of_mTrackers_2() { return &___mTrackers_2; }
	inline void set_mTrackers_2(Dictionary_2_tB17ABD9DDE1EDA398BB0CCCFED6756493B866D03 * value)
	{
		___mTrackers_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackers_2), value);
	}

	inline static int32_t get_offset_of_mTrackerCreators_3() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530, ___mTrackerCreators_3)); }
	inline Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775 * get_mTrackerCreators_3() const { return ___mTrackerCreators_3; }
	inline Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775 ** get_address_of_mTrackerCreators_3() { return &___mTrackerCreators_3; }
	inline void set_mTrackerCreators_3(Dictionary_2_t633AA66B66AB19AA7FDC21111B1C74DCE1079775 * value)
	{
		___mTrackerCreators_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerCreators_3), value);
	}

	inline static int32_t get_offset_of_mTrackerNativeDeinitializers_4() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530, ___mTrackerNativeDeinitializers_4)); }
	inline Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9 * get_mTrackerNativeDeinitializers_4() const { return ___mTrackerNativeDeinitializers_4; }
	inline Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9 ** get_address_of_mTrackerNativeDeinitializers_4() { return &___mTrackerNativeDeinitializers_4; }
	inline void set_mTrackerNativeDeinitializers_4(Dictionary_2_t7534CF3FD31278CAF9684333AD1265CDF6DE26C9 * value)
	{
		___mTrackerNativeDeinitializers_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerNativeDeinitializers_4), value);
	}
};

struct TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530_StaticFields
{
public:
	// Vuforia.ITrackerManager Vuforia.TrackerManager::mInstance
	RuntimeObject* ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530_StaticFields, ___mInstance_0)); }
	inline RuntimeObject* get_mInstance_0() const { return ___mInstance_0; }
	inline RuntimeObject** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(RuntimeObject* value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERMANAGER_T4E97F9E410AEF74F864668111D4E1AE7B8EA7530_H
#ifndef U3CU3EC_TB15984925A821B29BE48F5B2F3E523834E5E9370_H
#define U3CU3EC_TB15984925A821B29BE48F5B2F3E523834E5E9370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerManager/<>c
struct  U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields
{
public:
	// Vuforia.TrackerManager/<>c Vuforia.TrackerManager/<>c::<>9
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370 * ___U3CU3E9_0;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager/<>c::<>9__8_0
	Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * ___U3CU3E9__8_0_1;
	// System.Func`2<System.Type,Vuforia.Tracker> Vuforia.TrackerManager/<>c::<>9__8_1
	Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * ___U3CU3E9__8_1_2;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager/<>c::<>9__8_2
	Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * ___U3CU3E9__8_2_3;
	// System.Func`2<Vuforia.Tracker,System.Boolean> Vuforia.TrackerManager/<>c::<>9__8_3
	Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * ___U3CU3E9__8_3_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9__8_1_2)); }
	inline Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * get_U3CU3E9__8_1_2() const { return ___U3CU3E9__8_1_2; }
	inline Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 ** get_address_of_U3CU3E9__8_1_2() { return &___U3CU3E9__8_1_2; }
	inline void set_U3CU3E9__8_1_2(Func_2_t691A9ED4F0D625349D9866E4EA91A61D7B9E3580 * value)
	{
		___U3CU3E9__8_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9__8_2_3)); }
	inline Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * get_U3CU3E9__8_2_3() const { return ___U3CU3E9__8_2_3; }
	inline Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 ** get_address_of_U3CU3E9__8_2_3() { return &___U3CU3E9__8_2_3; }
	inline void set_U3CU3E9__8_2_3(Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * value)
	{
		___U3CU3E9__8_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields, ___U3CU3E9__8_3_4)); }
	inline Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * get_U3CU3E9__8_3_4() const { return ___U3CU3E9__8_3_4; }
	inline Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 ** get_address_of_U3CU3E9__8_3_4() { return &___U3CU3E9__8_3_4; }
	inline void set_U3CU3E9__8_3_4(Func_2_t44C14AE52E6939568B10B2D7FB7CE3C270666608 * value)
	{
		___U3CU3E9__8_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB15984925A821B29BE48F5B2F3E523834E5E9370_H
#ifndef U3CU3EC_T7F5A80A4ED3D4E5058582D2682AC998995E82FDB_H
#define U3CU3EC_T7F5A80A4ED3D4E5058582D2682AC998995E82FDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController/<>c
struct  U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields
{
public:
	// Vuforia.WebCamARController/<>c Vuforia.WebCamARController/<>c::<>9
	U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB * ___U3CU3E9_0;
	// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController/<>c::<>9__7_0
	Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7F5A80A4ED3D4E5058582D2682AC998995E82FDB_H
#ifndef U3CHOLEUPDATEU3ED__12_TE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F_H
#define U3CHOLEUPDATEU3ED__12_TE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_BulletHoleDecal/<holeUpdate>d__12
struct  U3CholeUpdateU3Ed__12_tE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F  : public RuntimeObject
{
public:
	// System.Int32 WFX_BulletHoleDecal/<holeUpdate>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WFX_BulletHoleDecal/<holeUpdate>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WFX_BulletHoleDecal WFX_BulletHoleDecal/<holeUpdate>d__12::<>4__this
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CholeUpdateU3Ed__12_tE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CholeUpdateU3Ed__12_tE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CholeUpdateU3Ed__12_tE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F, ___U3CU3E4__this_2)); }
	inline WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHOLEUPDATEU3ED__12_TE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F_H
#ifndef U3CRANDOMSPAWNSCOROUTINEU3ED__30_T529E8BF318623FFB6193BA10C31B081E1442B941_H
#define U3CRANDOMSPAWNSCOROUTINEU3ED__30_T529E8BF318623FFB6193BA10C31B081E1442B941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_Demo/<RandomSpawnsCoroutine>d__30
struct  U3CRandomSpawnsCoroutineU3Ed__30_t529E8BF318623FFB6193BA10C31B081E1442B941  : public RuntimeObject
{
public:
	// System.Int32 WFX_Demo/<RandomSpawnsCoroutine>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WFX_Demo/<RandomSpawnsCoroutine>d__30::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WFX_Demo WFX_Demo/<RandomSpawnsCoroutine>d__30::<>4__this
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRandomSpawnsCoroutineU3Ed__30_t529E8BF318623FFB6193BA10C31B081E1442B941, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRandomSpawnsCoroutineU3Ed__30_t529E8BF318623FFB6193BA10C31B081E1442B941, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRandomSpawnsCoroutineU3Ed__30_t529E8BF318623FFB6193BA10C31B081E1442B941, ___U3CU3E4__this_2)); }
	inline WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRANDOMSPAWNSCOROUTINEU3ED__30_T529E8BF318623FFB6193BA10C31B081E1442B941_H
#ifndef U3CCHECKFORDELETEDPARTICLESU3ED__41_T159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9_H
#define U3CCHECKFORDELETEDPARTICLESU3ED__41_T159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_Demo_New/<CheckForDeletedParticles>d__41
struct  U3CCheckForDeletedParticlesU3Ed__41_t159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9  : public RuntimeObject
{
public:
	// System.Int32 WFX_Demo_New/<CheckForDeletedParticles>d__41::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WFX_Demo_New/<CheckForDeletedParticles>d__41::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WFX_Demo_New WFX_Demo_New/<CheckForDeletedParticles>d__41::<>4__this
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCheckForDeletedParticlesU3Ed__41_t159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCheckForDeletedParticlesU3Ed__41_t159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCheckForDeletedParticlesU3Ed__41_t159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9, ___U3CU3E4__this_2)); }
	inline WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKFORDELETEDPARTICLESU3ED__41_T159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9_H
#ifndef U3CFLICKERU3ED__3_TDBCB5374078529EF8B3F7BC65291F8A27D0EADE7_H
#define U3CFLICKERU3ED__3_TDBCB5374078529EF8B3F7BC65291F8A27D0EADE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_LightFlicker/<Flicker>d__3
struct  U3CFlickerU3Ed__3_tDBCB5374078529EF8B3F7BC65291F8A27D0EADE7  : public RuntimeObject
{
public:
	// System.Int32 WFX_LightFlicker/<Flicker>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WFX_LightFlicker/<Flicker>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WFX_LightFlicker WFX_LightFlicker/<Flicker>d__3::<>4__this
	WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ed__3_tDBCB5374078529EF8B3F7BC65291F8A27D0EADE7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ed__3_tDBCB5374078529EF8B3F7BC65291F8A27D0EADE7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ed__3_tDBCB5374078529EF8B3F7BC65291F8A27D0EADE7, ___U3CU3E4__this_2)); }
	inline WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLICKERU3ED__3_TDBCB5374078529EF8B3F7BC65291F8A27D0EADE7_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_TD7B241978857C817DC5F90FD7E79EBA548903A7E_H
#define __STATICARRAYINITTYPESIZEU3D24_TD7B241978857C817DC5F90FD7E79EBA548903A7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct  __StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_TD7B241978857C817DC5F90FD7E79EBA548903A7E_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#define RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleData
#pragma pack(push, tp, 1)
struct  RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9 
{
public:
	// System.Single Vuforia.RectangleData::leftTopX
	float ___leftTopX_0;
	// System.Single Vuforia.RectangleData::leftTopY
	float ___leftTopY_1;
	// System.Single Vuforia.RectangleData::rightBottomX
	float ___rightBottomX_2;
	// System.Single Vuforia.RectangleData::rightBottomY
	float ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___leftTopX_0)); }
	inline float get_leftTopX_0() const { return ___leftTopX_0; }
	inline float* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(float value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___leftTopY_1)); }
	inline float get_leftTopY_1() const { return ___leftTopY_1; }
	inline float* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(float value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___rightBottomX_2)); }
	inline float get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline float* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(float value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___rightBottomY_3)); }
	inline float get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline float* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(float value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#ifndef WEBCAMARCONTROLLER_T927205B62F0EDCE72569FA4CA0D034E6BC804D1A_H
#define WEBCAMARCONTROLLER_T927205B62F0EDCE72569FA4CA0D034E6BC804D1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamARController
struct  WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// System.Int32 Vuforia.WebCamARController::RenderTextureLayer
	int32_t ___RenderTextureLayer_1;
	// System.String Vuforia.WebCamARController::mDeviceNameSetInEditor
	String_t* ___mDeviceNameSetInEditor_2;
	// System.Boolean Vuforia.WebCamARController::mFlipHorizontally
	bool ___mFlipHorizontally_3;
	// Vuforia.WebCam Vuforia.WebCamARController::mWebCamImpl
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2 * ___mWebCamImpl_4;
	// System.Func`3<System.String,Vuforia.WebCamProfile/ProfileData,Vuforia.IWebCamTexAdaptor> Vuforia.WebCamARController::mWebCamTexAdaptorProvider
	Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * ___mWebCamTexAdaptorProvider_5;

public:
	inline static int32_t get_offset_of_RenderTextureLayer_1() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___RenderTextureLayer_1)); }
	inline int32_t get_RenderTextureLayer_1() const { return ___RenderTextureLayer_1; }
	inline int32_t* get_address_of_RenderTextureLayer_1() { return &___RenderTextureLayer_1; }
	inline void set_RenderTextureLayer_1(int32_t value)
	{
		___RenderTextureLayer_1 = value;
	}

	inline static int32_t get_offset_of_mDeviceNameSetInEditor_2() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___mDeviceNameSetInEditor_2)); }
	inline String_t* get_mDeviceNameSetInEditor_2() const { return ___mDeviceNameSetInEditor_2; }
	inline String_t** get_address_of_mDeviceNameSetInEditor_2() { return &___mDeviceNameSetInEditor_2; }
	inline void set_mDeviceNameSetInEditor_2(String_t* value)
	{
		___mDeviceNameSetInEditor_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceNameSetInEditor_2), value);
	}

	inline static int32_t get_offset_of_mFlipHorizontally_3() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___mFlipHorizontally_3)); }
	inline bool get_mFlipHorizontally_3() const { return ___mFlipHorizontally_3; }
	inline bool* get_address_of_mFlipHorizontally_3() { return &___mFlipHorizontally_3; }
	inline void set_mFlipHorizontally_3(bool value)
	{
		___mFlipHorizontally_3 = value;
	}

	inline static int32_t get_offset_of_mWebCamImpl_4() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___mWebCamImpl_4)); }
	inline WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2 * get_mWebCamImpl_4() const { return ___mWebCamImpl_4; }
	inline WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2 ** get_address_of_mWebCamImpl_4() { return &___mWebCamImpl_4; }
	inline void set_mWebCamImpl_4(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2 * value)
	{
		___mWebCamImpl_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamImpl_4), value);
	}

	inline static int32_t get_offset_of_mWebCamTexAdaptorProvider_5() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A, ___mWebCamTexAdaptorProvider_5)); }
	inline Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * get_mWebCamTexAdaptorProvider_5() const { return ___mWebCamTexAdaptorProvider_5; }
	inline Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC ** get_address_of_mWebCamTexAdaptorProvider_5() { return &___mWebCamTexAdaptorProvider_5; }
	inline void set_mWebCamTexAdaptorProvider_5(Func_3_t68B3AD5FB9C05CA22D6D4757BCA89F998A1EB5AC * value)
	{
		___mWebCamTexAdaptorProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexAdaptorProvider_5), value);
	}
};

struct WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields
{
public:
	// Vuforia.WebCamARController Vuforia.WebCamARController::mInstance
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A * ___mInstance_6;
	// System.Object Vuforia.WebCamARController::mPadlock
	RuntimeObject * ___mPadlock_7;

public:
	inline static int32_t get_offset_of_mInstance_6() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields, ___mInstance_6)); }
	inline WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A * get_mInstance_6() const { return ___mInstance_6; }
	inline WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A ** get_address_of_mInstance_6() { return &___mInstance_6; }
	inline void set_mInstance_6(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A * value)
	{
		___mInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_6), value);
	}

	inline static int32_t get_offset_of_mPadlock_7() { return static_cast<int32_t>(offsetof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields, ___mPadlock_7)); }
	inline RuntimeObject * get_mPadlock_7() const { return ___mPadlock_7; }
	inline RuntimeObject ** get_address_of_mPadlock_7() { return &___mPadlock_7; }
	inline void set_mPadlock_7(RuntimeObject * value)
	{
		___mPadlock_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMARCONTROLLER_T927205B62F0EDCE72569FA4CA0D034E6BC804D1A_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6817CFECEB78EAB6A13B460969C0CEAD9F899C47_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6817CFECEB78EAB6A13B460969C0CEAD9F899C47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::898C2022A0C02FCE602BF05E1C09BD48301606E5
	__StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E  ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0;

public:
	inline static int32_t get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47_StaticFields, ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0)); }
	inline __StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E  get_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() const { return ___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline __StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E * get_address_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0() { return &___898C2022A0C02FCE602BF05E1C09BD48301606E5_0; }
	inline void set_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(__StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E  value)
	{
		___898C2022A0C02FCE602BF05E1C09BD48301606E5_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6817CFECEB78EAB6A13B460969C0CEAD9F899C47_H
#ifndef PARTICLEEXAMPLES_T26D5EB932FF7F2D069A066370B9340604A938190_H
#define PARTICLEEXAMPLES_T26D5EB932FF7F2D069A066370B9340604A938190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleExamples
struct  ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190  : public RuntimeObject
{
public:
	// System.String ParticleExamples::title
	String_t* ___title_0;
	// System.String ParticleExamples::description
	String_t* ___description_1;
	// System.Boolean ParticleExamples::isWeaponEffect
	bool ___isWeaponEffect_2;
	// UnityEngine.GameObject ParticleExamples::particleSystemGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___particleSystemGO_3;
	// UnityEngine.Vector3 ParticleExamples::particlePosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___particlePosition_4;
	// UnityEngine.Vector3 ParticleExamples::particleRotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___particleRotation_5;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier((&___title_0), value);
	}

	inline static int32_t get_offset_of_description_1() { return static_cast<int32_t>(offsetof(ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190, ___description_1)); }
	inline String_t* get_description_1() const { return ___description_1; }
	inline String_t** get_address_of_description_1() { return &___description_1; }
	inline void set_description_1(String_t* value)
	{
		___description_1 = value;
		Il2CppCodeGenWriteBarrier((&___description_1), value);
	}

	inline static int32_t get_offset_of_isWeaponEffect_2() { return static_cast<int32_t>(offsetof(ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190, ___isWeaponEffect_2)); }
	inline bool get_isWeaponEffect_2() const { return ___isWeaponEffect_2; }
	inline bool* get_address_of_isWeaponEffect_2() { return &___isWeaponEffect_2; }
	inline void set_isWeaponEffect_2(bool value)
	{
		___isWeaponEffect_2 = value;
	}

	inline static int32_t get_offset_of_particleSystemGO_3() { return static_cast<int32_t>(offsetof(ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190, ___particleSystemGO_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_particleSystemGO_3() const { return ___particleSystemGO_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_particleSystemGO_3() { return &___particleSystemGO_3; }
	inline void set_particleSystemGO_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___particleSystemGO_3 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystemGO_3), value);
	}

	inline static int32_t get_offset_of_particlePosition_4() { return static_cast<int32_t>(offsetof(ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190, ___particlePosition_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_particlePosition_4() const { return ___particlePosition_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_particlePosition_4() { return &___particlePosition_4; }
	inline void set_particlePosition_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___particlePosition_4 = value;
	}

	inline static int32_t get_offset_of_particleRotation_5() { return static_cast<int32_t>(offsetof(ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190, ___particleRotation_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_particleRotation_5() const { return ___particleRotation_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_particleRotation_5() { return &___particleRotation_5; }
	inline void set_particleRotation_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___particleRotation_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEEXAMPLES_T26D5EB932FF7F2D069A066370B9340604A938190_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#define CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaUtility/CLIPPING_MODE
struct  CLIPPING_MODE_t74517F84F38E21AC75F4DB3A1D76698EA0D2580D 
{
public:
	// System.Int32 Vuforia.HideExcessAreaUtility/CLIPPING_MODE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t74517F84F38E21AC75F4DB3A1D76698EA0D2580D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#ifndef FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#define FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder/FrameQuality
struct  FrameQuality_tEA002835E0F464BB9506076B691826DAAFEB87FB 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder/FrameQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FrameQuality_tEA002835E0F464BB9506076B691826DAAFEB87FB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#ifndef STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#define STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifndef SENSITIVITY_T7654EFB20C36491C60EAA2D010FAA6A0D9D14B01_H
#define SENSITIVITY_T7654EFB20C36491C60EAA2D010FAA6A0D9D14B01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton/Sensitivity
struct  Sensitivity_t7654EFB20C36491C60EAA2D010FAA6A0D9D14B01 
{
public:
	// System.Int32 Vuforia.VirtualButton/Sensitivity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Sensitivity_t7654EFB20C36491C60EAA2D010FAA6A0D9D14B01, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENSITIVITY_T7654EFB20C36491C60EAA2D010FAA6A0D9D14B01_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef VIDEOBACKGROUNDMANAGER_TDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_H
#define VIDEOBACKGROUNDMANAGER_TDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundManager
struct  VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// Vuforia.HideExcessAreaUtility/CLIPPING_MODE Vuforia.VideoBackgroundManager::mClippingMode
	int32_t ___mClippingMode_1;
	// UnityEngine.Shader Vuforia.VideoBackgroundManager::mMatteShader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___mMatteShader_2;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBackgroundEnabled
	bool ___mVideoBackgroundEnabled_3;
	// UnityEngine.Texture Vuforia.VideoBackgroundManager::mTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___mTexture_4;
	// System.Boolean Vuforia.VideoBackgroundManager::mVideoBgConfigChanged
	bool ___mVideoBgConfigChanged_5;
	// System.IntPtr Vuforia.VideoBackgroundManager::mNativeTexturePtr
	intptr_t ___mNativeTexturePtr_6;

public:
	inline static int32_t get_offset_of_mClippingMode_1() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mClippingMode_1)); }
	inline int32_t get_mClippingMode_1() const { return ___mClippingMode_1; }
	inline int32_t* get_address_of_mClippingMode_1() { return &___mClippingMode_1; }
	inline void set_mClippingMode_1(int32_t value)
	{
		___mClippingMode_1 = value;
	}

	inline static int32_t get_offset_of_mMatteShader_2() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mMatteShader_2)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_mMatteShader_2() const { return ___mMatteShader_2; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_mMatteShader_2() { return &___mMatteShader_2; }
	inline void set_mMatteShader_2(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___mMatteShader_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundEnabled_3() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mVideoBackgroundEnabled_3)); }
	inline bool get_mVideoBackgroundEnabled_3() const { return ___mVideoBackgroundEnabled_3; }
	inline bool* get_address_of_mVideoBackgroundEnabled_3() { return &___mVideoBackgroundEnabled_3; }
	inline void set_mVideoBackgroundEnabled_3(bool value)
	{
		___mVideoBackgroundEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mTexture_4() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mTexture_4)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_mTexture_4() const { return ___mTexture_4; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_mTexture_4() { return &___mTexture_4; }
	inline void set_mTexture_4(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___mTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgConfigChanged_5() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mVideoBgConfigChanged_5)); }
	inline bool get_mVideoBgConfigChanged_5() const { return ___mVideoBgConfigChanged_5; }
	inline bool* get_address_of_mVideoBgConfigChanged_5() { return &___mVideoBgConfigChanged_5; }
	inline void set_mVideoBgConfigChanged_5(bool value)
	{
		___mVideoBgConfigChanged_5 = value;
	}

	inline static int32_t get_offset_of_mNativeTexturePtr_6() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50, ___mNativeTexturePtr_6)); }
	inline intptr_t get_mNativeTexturePtr_6() const { return ___mNativeTexturePtr_6; }
	inline intptr_t* get_address_of_mNativeTexturePtr_6() { return &___mNativeTexturePtr_6; }
	inline void set_mNativeTexturePtr_6(intptr_t value)
	{
		___mNativeTexturePtr_6 = value;
	}
};

struct VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields
{
public:
	// Vuforia.VideoBackgroundManager Vuforia.VideoBackgroundManager::mInstance
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50 * ___mInstance_7;
	// System.Object Vuforia.VideoBackgroundManager::mPadlock
	RuntimeObject * ___mPadlock_8;

public:
	inline static int32_t get_offset_of_mInstance_7() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields, ___mInstance_7)); }
	inline VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50 * get_mInstance_7() const { return ___mInstance_7; }
	inline VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50 ** get_address_of_mInstance_7() { return &___mInstance_7; }
	inline void set_mInstance_7(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50 * value)
	{
		___mInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_7), value);
	}

	inline static int32_t get_offset_of_mPadlock_8() { return static_cast<int32_t>(offsetof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields, ___mPadlock_8)); }
	inline RuntimeObject * get_mPadlock_8() const { return ___mPadlock_8; }
	inline RuntimeObject ** get_address_of_mPadlock_8() { return &___mPadlock_8; }
	inline void set_mPadlock_8(RuntimeObject * value)
	{
		___mPadlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDMANAGER_TDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_H
#ifndef VIRTUALBUTTON_T95A02A5A354F3D3274398C6CEE3CC0E96E38D654_H
#define VIRTUALBUTTON_T95A02A5A354F3D3274398C6CEE3CC0E96E38D654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButton
struct  VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654  : public RuntimeObject
{
public:
	// System.String Vuforia.VirtualButton::mName
	String_t* ___mName_0;
	// System.Int32 Vuforia.VirtualButton::mID
	int32_t ___mID_1;
	// Vuforia.RectangleData Vuforia.VirtualButton::mArea
	RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9  ___mArea_2;
	// System.Boolean Vuforia.VirtualButton::mIsEnabled
	bool ___mIsEnabled_3;
	// Vuforia.ImageTarget Vuforia.VirtualButton::mParentImageTarget
	RuntimeObject* ___mParentImageTarget_4;
	// Vuforia.DataSet Vuforia.VirtualButton::mParentDataSet
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * ___mParentDataSet_5;

public:
	inline static int32_t get_offset_of_mName_0() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mName_0)); }
	inline String_t* get_mName_0() const { return ___mName_0; }
	inline String_t** get_address_of_mName_0() { return &___mName_0; }
	inline void set_mName_0(String_t* value)
	{
		___mName_0 = value;
		Il2CppCodeGenWriteBarrier((&___mName_0), value);
	}

	inline static int32_t get_offset_of_mID_1() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mID_1)); }
	inline int32_t get_mID_1() const { return ___mID_1; }
	inline int32_t* get_address_of_mID_1() { return &___mID_1; }
	inline void set_mID_1(int32_t value)
	{
		___mID_1 = value;
	}

	inline static int32_t get_offset_of_mArea_2() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mArea_2)); }
	inline RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9  get_mArea_2() const { return ___mArea_2; }
	inline RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9 * get_address_of_mArea_2() { return &___mArea_2; }
	inline void set_mArea_2(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9  value)
	{
		___mArea_2 = value;
	}

	inline static int32_t get_offset_of_mIsEnabled_3() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mIsEnabled_3)); }
	inline bool get_mIsEnabled_3() const { return ___mIsEnabled_3; }
	inline bool* get_address_of_mIsEnabled_3() { return &___mIsEnabled_3; }
	inline void set_mIsEnabled_3(bool value)
	{
		___mIsEnabled_3 = value;
	}

	inline static int32_t get_offset_of_mParentImageTarget_4() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mParentImageTarget_4)); }
	inline RuntimeObject* get_mParentImageTarget_4() const { return ___mParentImageTarget_4; }
	inline RuntimeObject** get_address_of_mParentImageTarget_4() { return &___mParentImageTarget_4; }
	inline void set_mParentImageTarget_4(RuntimeObject* value)
	{
		___mParentImageTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___mParentImageTarget_4), value);
	}

	inline static int32_t get_offset_of_mParentDataSet_5() { return static_cast<int32_t>(offsetof(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654, ___mParentDataSet_5)); }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * get_mParentDataSet_5() const { return ___mParentDataSet_5; }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 ** get_address_of_mParentDataSet_5() { return &___mParentDataSet_5; }
	inline void set_mParentDataSet_5(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * value)
	{
		___mParentDataSet_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentDataSet_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTON_T95A02A5A354F3D3274398C6CEE3CC0E96E38D654_H
#ifndef README_T3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46_H
#define README_T3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Readme
struct  Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Texture2D Readme::icon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___icon_4;
	// System.String Readme::title
	String_t* ___title_5;
	// Readme/Section[] Readme::sections
	SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364* ___sections_6;
	// System.Boolean Readme::loadedLayout
	bool ___loadedLayout_7;

public:
	inline static int32_t get_offset_of_icon_4() { return static_cast<int32_t>(offsetof(Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46, ___icon_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_icon_4() const { return ___icon_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_icon_4() { return &___icon_4; }
	inline void set_icon_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___icon_4 = value;
		Il2CppCodeGenWriteBarrier((&___icon_4), value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier((&___title_5), value);
	}

	inline static int32_t get_offset_of_sections_6() { return static_cast<int32_t>(offsetof(Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46, ___sections_6)); }
	inline SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364* get_sections_6() const { return ___sections_6; }
	inline SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364** get_address_of_sections_6() { return &___sections_6; }
	inline void set_sections_6(SectionU5BU5D_t3EF1EA79AB7297D95C599BBA1E7D1DF5D69B5364* value)
	{
		___sections_6 = value;
		Il2CppCodeGenWriteBarrier((&___sections_6), value);
	}

	inline static int32_t get_offset_of_loadedLayout_7() { return static_cast<int32_t>(offsetof(Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46, ___loadedLayout_7)); }
	inline bool get_loadedLayout_7() const { return ___loadedLayout_7; }
	inline bool* get_address_of_loadedLayout_7() { return &___loadedLayout_7; }
	inline void set_loadedLayout_7(bool value)
	{
		___loadedLayout_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // README_T3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ANIMATEONTRACK_TC38FAC3212614A222F7ED44E3A59F28CCD6F12D4_H
#define ANIMATEONTRACK_TC38FAC3212614A222F7ED44E3A59F28CCD6F12D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimateOnTrack
struct  AnimateOnTrack_tC38FAC3212614A222F7ED44E3A59F28CCD6F12D4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject AnimateOnTrack::trackableObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___trackableObject_4;
	// Vuforia.TrackableBehaviour AnimateOnTrack::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_5;
	// UnityEngine.Animator AnimateOnTrack::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_6;

public:
	inline static int32_t get_offset_of_trackableObject_4() { return static_cast<int32_t>(offsetof(AnimateOnTrack_tC38FAC3212614A222F7ED44E3A59F28CCD6F12D4, ___trackableObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_trackableObject_4() const { return ___trackableObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_trackableObject_4() { return &___trackableObject_4; }
	inline void set_trackableObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___trackableObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___trackableObject_4), value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_5() { return static_cast<int32_t>(offsetof(AnimateOnTrack_tC38FAC3212614A222F7ED44E3A59F28CCD6F12D4, ___mTrackableBehaviour_5)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_5() const { return ___mTrackableBehaviour_5; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_5() { return &___mTrackableBehaviour_5; }
	inline void set_mTrackableBehaviour_5(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_5), value);
	}

	inline static int32_t get_offset_of_anim_6() { return static_cast<int32_t>(offsetof(AnimateOnTrack_tC38FAC3212614A222F7ED44E3A59F28CCD6F12D4, ___anim_6)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_6() const { return ___anim_6; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_6() { return &___anim_6; }
	inline void set_anim_6(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_6 = value;
		Il2CppCodeGenWriteBarrier((&___anim_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEONTRACK_TC38FAC3212614A222F7ED44E3A59F28CCD6F12D4_H
#ifndef BACKEXIT_T4B9D835283F8D0BFCF046C8CD8E88AED2DAA1871_H
#define BACKEXIT_T4B9D835283F8D0BFCF046C8CD8E88AED2DAA1871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackExit
struct  BackExit_t4B9D835283F8D0BFCF046C8CD8E88AED2DAA1871  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKEXIT_T4B9D835283F8D0BFCF046C8CD8E88AED2DAA1871_H
#ifndef CFX_AUTODESTRUCTSHURIKEN_TF656F157683CC094608046496754C72A910D10F5_H
#define CFX_AUTODESTRUCTSHURIKEN_TF656F157683CC094608046496754C72A910D10F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutoDestructShuriken
struct  CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CFX_AutoDestructShuriken::OnlyDeactivate
	bool ___OnlyDeactivate_4;

public:
	inline static int32_t get_offset_of_OnlyDeactivate_4() { return static_cast<int32_t>(offsetof(CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5, ___OnlyDeactivate_4)); }
	inline bool get_OnlyDeactivate_4() const { return ___OnlyDeactivate_4; }
	inline bool* get_address_of_OnlyDeactivate_4() { return &___OnlyDeactivate_4; }
	inline void set_OnlyDeactivate_4(bool value)
	{
		___OnlyDeactivate_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_AUTODESTRUCTSHURIKEN_TF656F157683CC094608046496754C72A910D10F5_H
#ifndef CFX_AUTOSTOPLOOPEDEFFECT_TDF281CCB89367AEE38D90CDF62617F85A9316A7F_H
#define CFX_AUTOSTOPLOOPEDEFFECT_TDF281CCB89367AEE38D90CDF62617F85A9316A7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutoStopLoopedEffect
struct  CFX_AutoStopLoopedEffect_tDF281CCB89367AEE38D90CDF62617F85A9316A7F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CFX_AutoStopLoopedEffect::effectDuration
	float ___effectDuration_4;
	// System.Single CFX_AutoStopLoopedEffect::d
	float ___d_5;

public:
	inline static int32_t get_offset_of_effectDuration_4() { return static_cast<int32_t>(offsetof(CFX_AutoStopLoopedEffect_tDF281CCB89367AEE38D90CDF62617F85A9316A7F, ___effectDuration_4)); }
	inline float get_effectDuration_4() const { return ___effectDuration_4; }
	inline float* get_address_of_effectDuration_4() { return &___effectDuration_4; }
	inline void set_effectDuration_4(float value)
	{
		___effectDuration_4 = value;
	}

	inline static int32_t get_offset_of_d_5() { return static_cast<int32_t>(offsetof(CFX_AutoStopLoopedEffect_tDF281CCB89367AEE38D90CDF62617F85A9316A7F, ___d_5)); }
	inline float get_d_5() const { return ___d_5; }
	inline float* get_address_of_d_5() { return &___d_5; }
	inline void set_d_5(float value)
	{
		___d_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_AUTOSTOPLOOPEDEFFECT_TDF281CCB89367AEE38D90CDF62617F85A9316A7F_H
#ifndef CFX_DEMO_RANDOMDIR_T60CF3850799F52A2210DA9A0434DFAB5D88D07CC_H
#define CFX_DEMO_RANDOMDIR_T60CF3850799F52A2210DA9A0434DFAB5D88D07CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_RandomDir
struct  CFX_Demo_RandomDir_t60CF3850799F52A2210DA9A0434DFAB5D88D07CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 CFX_Demo_RandomDir::min
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___min_4;
	// UnityEngine.Vector3 CFX_Demo_RandomDir::max
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___max_5;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDir_t60CF3850799F52A2210DA9A0434DFAB5D88D07CC, ___min_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_min_4() const { return ___min_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(CFX_Demo_RandomDir_t60CF3850799F52A2210DA9A0434DFAB5D88D07CC, ___max_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_max_5() const { return ___max_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___max_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_DEMO_RANDOMDIR_T60CF3850799F52A2210DA9A0434DFAB5D88D07CC_H
#ifndef CFX_DEMO_ROTATECAMERA_TB84DB0DAACDDD78C6BBDDB4365EC775B90961354_H
#define CFX_DEMO_ROTATECAMERA_TB84DB0DAACDDD78C6BBDDB4365EC775B90961354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_RotateCamera
struct  CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CFX_Demo_RotateCamera::speed
	float ___speed_5;
	// UnityEngine.Transform CFX_Demo_RotateCamera::rotationCenter
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___rotationCenter_6;

public:
	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_rotationCenter_6() { return static_cast<int32_t>(offsetof(CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354, ___rotationCenter_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_rotationCenter_6() const { return ___rotationCenter_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_rotationCenter_6() { return &___rotationCenter_6; }
	inline void set_rotationCenter_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___rotationCenter_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotationCenter_6), value);
	}
};

struct CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354_StaticFields
{
public:
	// System.Boolean CFX_Demo_RotateCamera::rotating
	bool ___rotating_4;

public:
	inline static int32_t get_offset_of_rotating_4() { return static_cast<int32_t>(offsetof(CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354_StaticFields, ___rotating_4)); }
	inline bool get_rotating_4() const { return ___rotating_4; }
	inline bool* get_address_of_rotating_4() { return &___rotating_4; }
	inline void set_rotating_4(bool value)
	{
		___rotating_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_DEMO_ROTATECAMERA_TB84DB0DAACDDD78C6BBDDB4365EC775B90961354_H
#ifndef CFX_DEMO_TRANSLATE_T62611C2FF6F408278CE320E134FC59F15E5C3D3F_H
#define CFX_DEMO_TRANSLATE_T62611C2FF6F408278CE320E134FC59F15E5C3D3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_Demo_Translate
struct  CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CFX_Demo_Translate::speed
	float ___speed_4;
	// UnityEngine.Vector3 CFX_Demo_Translate::rotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rotation_5;
	// UnityEngine.Vector3 CFX_Demo_Translate::axis
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___axis_6;
	// System.Boolean CFX_Demo_Translate::gravity
	bool ___gravity_7;
	// UnityEngine.Vector3 CFX_Demo_Translate::dir
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dir_8;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_rotation_5() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F, ___rotation_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rotation_5() const { return ___rotation_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rotation_5() { return &___rotation_5; }
	inline void set_rotation_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rotation_5 = value;
	}

	inline static int32_t get_offset_of_axis_6() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F, ___axis_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_axis_6() const { return ___axis_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_axis_6() { return &___axis_6; }
	inline void set_axis_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___axis_6 = value;
	}

	inline static int32_t get_offset_of_gravity_7() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F, ___gravity_7)); }
	inline bool get_gravity_7() const { return ___gravity_7; }
	inline bool* get_address_of_gravity_7() { return &___gravity_7; }
	inline void set_gravity_7(bool value)
	{
		___gravity_7 = value;
	}

	inline static int32_t get_offset_of_dir_8() { return static_cast<int32_t>(offsetof(CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F, ___dir_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_dir_8() const { return ___dir_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_dir_8() { return &___dir_8; }
	inline void set_dir_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___dir_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_DEMO_TRANSLATE_T62611C2FF6F408278CE320E134FC59F15E5C3D3F_H
#ifndef CFX_LIGHTINTENSITYFADE_T0AC70576F12AE545F5A72AF67C99CDE6711B3C12_H
#define CFX_LIGHTINTENSITYFADE_T0AC70576F12AE545F5A72AF67C99CDE6711B3C12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_LightIntensityFade
struct  CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CFX_LightIntensityFade::duration
	float ___duration_4;
	// System.Single CFX_LightIntensityFade::delay
	float ___delay_5;
	// System.Single CFX_LightIntensityFade::finalIntensity
	float ___finalIntensity_6;
	// System.Single CFX_LightIntensityFade::baseIntensity
	float ___baseIntensity_7;
	// System.Boolean CFX_LightIntensityFade::autodestruct
	bool ___autodestruct_8;
	// System.Single CFX_LightIntensityFade::p_lifetime
	float ___p_lifetime_9;
	// System.Single CFX_LightIntensityFade::p_delay
	float ___p_delay_10;

public:
	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_delay_5() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12, ___delay_5)); }
	inline float get_delay_5() const { return ___delay_5; }
	inline float* get_address_of_delay_5() { return &___delay_5; }
	inline void set_delay_5(float value)
	{
		___delay_5 = value;
	}

	inline static int32_t get_offset_of_finalIntensity_6() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12, ___finalIntensity_6)); }
	inline float get_finalIntensity_6() const { return ___finalIntensity_6; }
	inline float* get_address_of_finalIntensity_6() { return &___finalIntensity_6; }
	inline void set_finalIntensity_6(float value)
	{
		___finalIntensity_6 = value;
	}

	inline static int32_t get_offset_of_baseIntensity_7() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12, ___baseIntensity_7)); }
	inline float get_baseIntensity_7() const { return ___baseIntensity_7; }
	inline float* get_address_of_baseIntensity_7() { return &___baseIntensity_7; }
	inline void set_baseIntensity_7(float value)
	{
		___baseIntensity_7 = value;
	}

	inline static int32_t get_offset_of_autodestruct_8() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12, ___autodestruct_8)); }
	inline bool get_autodestruct_8() const { return ___autodestruct_8; }
	inline bool* get_address_of_autodestruct_8() { return &___autodestruct_8; }
	inline void set_autodestruct_8(bool value)
	{
		___autodestruct_8 = value;
	}

	inline static int32_t get_offset_of_p_lifetime_9() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12, ___p_lifetime_9)); }
	inline float get_p_lifetime_9() const { return ___p_lifetime_9; }
	inline float* get_address_of_p_lifetime_9() { return &___p_lifetime_9; }
	inline void set_p_lifetime_9(float value)
	{
		___p_lifetime_9 = value;
	}

	inline static int32_t get_offset_of_p_delay_10() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12, ___p_delay_10)); }
	inline float get_p_delay_10() const { return ___p_delay_10; }
	inline float* get_address_of_p_delay_10() { return &___p_delay_10; }
	inline void set_p_delay_10(float value)
	{
		___p_delay_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_LIGHTINTENSITYFADE_T0AC70576F12AE545F5A72AF67C99CDE6711B3C12_H
#ifndef CFX_SPAWNSYSTEM_T47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973_H
#define CFX_SPAWNSYSTEM_T47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_SpawnSystem
struct  CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject[] CFX_SpawnSystem::objectsToPreload
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___objectsToPreload_5;
	// System.Int32[] CFX_SpawnSystem::objectsToPreloadTimes
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___objectsToPreloadTimes_6;
	// System.Boolean CFX_SpawnSystem::hideObjectsInHierarchy
	bool ___hideObjectsInHierarchy_7;
	// System.Boolean CFX_SpawnSystem::spawnAsChildren
	bool ___spawnAsChildren_8;
	// System.Boolean CFX_SpawnSystem::onlyGetInactiveObjects
	bool ___onlyGetInactiveObjects_9;
	// System.Boolean CFX_SpawnSystem::instantiateIfNeeded
	bool ___instantiateIfNeeded_10;
	// System.Boolean CFX_SpawnSystem::allObjectsLoaded
	bool ___allObjectsLoaded_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>> CFX_SpawnSystem::instantiatedObjects
	Dictionary_2_t758C8FD2F54C417732AA0EFD9E5CB03E9F88FEC5 * ___instantiatedObjects_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CFX_SpawnSystem::poolCursors
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___poolCursors_13;

public:
	inline static int32_t get_offset_of_objectsToPreload_5() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___objectsToPreload_5)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_objectsToPreload_5() const { return ___objectsToPreload_5; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_objectsToPreload_5() { return &___objectsToPreload_5; }
	inline void set_objectsToPreload_5(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___objectsToPreload_5 = value;
		Il2CppCodeGenWriteBarrier((&___objectsToPreload_5), value);
	}

	inline static int32_t get_offset_of_objectsToPreloadTimes_6() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___objectsToPreloadTimes_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_objectsToPreloadTimes_6() const { return ___objectsToPreloadTimes_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_objectsToPreloadTimes_6() { return &___objectsToPreloadTimes_6; }
	inline void set_objectsToPreloadTimes_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___objectsToPreloadTimes_6 = value;
		Il2CppCodeGenWriteBarrier((&___objectsToPreloadTimes_6), value);
	}

	inline static int32_t get_offset_of_hideObjectsInHierarchy_7() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___hideObjectsInHierarchy_7)); }
	inline bool get_hideObjectsInHierarchy_7() const { return ___hideObjectsInHierarchy_7; }
	inline bool* get_address_of_hideObjectsInHierarchy_7() { return &___hideObjectsInHierarchy_7; }
	inline void set_hideObjectsInHierarchy_7(bool value)
	{
		___hideObjectsInHierarchy_7 = value;
	}

	inline static int32_t get_offset_of_spawnAsChildren_8() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___spawnAsChildren_8)); }
	inline bool get_spawnAsChildren_8() const { return ___spawnAsChildren_8; }
	inline bool* get_address_of_spawnAsChildren_8() { return &___spawnAsChildren_8; }
	inline void set_spawnAsChildren_8(bool value)
	{
		___spawnAsChildren_8 = value;
	}

	inline static int32_t get_offset_of_onlyGetInactiveObjects_9() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___onlyGetInactiveObjects_9)); }
	inline bool get_onlyGetInactiveObjects_9() const { return ___onlyGetInactiveObjects_9; }
	inline bool* get_address_of_onlyGetInactiveObjects_9() { return &___onlyGetInactiveObjects_9; }
	inline void set_onlyGetInactiveObjects_9(bool value)
	{
		___onlyGetInactiveObjects_9 = value;
	}

	inline static int32_t get_offset_of_instantiateIfNeeded_10() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___instantiateIfNeeded_10)); }
	inline bool get_instantiateIfNeeded_10() const { return ___instantiateIfNeeded_10; }
	inline bool* get_address_of_instantiateIfNeeded_10() { return &___instantiateIfNeeded_10; }
	inline void set_instantiateIfNeeded_10(bool value)
	{
		___instantiateIfNeeded_10 = value;
	}

	inline static int32_t get_offset_of_allObjectsLoaded_11() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___allObjectsLoaded_11)); }
	inline bool get_allObjectsLoaded_11() const { return ___allObjectsLoaded_11; }
	inline bool* get_address_of_allObjectsLoaded_11() { return &___allObjectsLoaded_11; }
	inline void set_allObjectsLoaded_11(bool value)
	{
		___allObjectsLoaded_11 = value;
	}

	inline static int32_t get_offset_of_instantiatedObjects_12() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___instantiatedObjects_12)); }
	inline Dictionary_2_t758C8FD2F54C417732AA0EFD9E5CB03E9F88FEC5 * get_instantiatedObjects_12() const { return ___instantiatedObjects_12; }
	inline Dictionary_2_t758C8FD2F54C417732AA0EFD9E5CB03E9F88FEC5 ** get_address_of_instantiatedObjects_12() { return &___instantiatedObjects_12; }
	inline void set_instantiatedObjects_12(Dictionary_2_t758C8FD2F54C417732AA0EFD9E5CB03E9F88FEC5 * value)
	{
		___instantiatedObjects_12 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedObjects_12), value);
	}

	inline static int32_t get_offset_of_poolCursors_13() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973, ___poolCursors_13)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_poolCursors_13() const { return ___poolCursors_13; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_poolCursors_13() { return &___poolCursors_13; }
	inline void set_poolCursors_13(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___poolCursors_13 = value;
		Il2CppCodeGenWriteBarrier((&___poolCursors_13), value);
	}
};

struct CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973_StaticFields
{
public:
	// CFX_SpawnSystem CFX_SpawnSystem::instance
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973_StaticFields, ___instance_4)); }
	inline CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973 * get_instance_4() const { return ___instance_4; }
	inline CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_SPAWNSYSTEM_T47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973_H
#ifndef CONGUI_TE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84_H
#define CONGUI_TE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConGUI
struct  ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform ConGUI::mainCamera
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mainCamera_4;
	// UnityEngine.Transform ConGUI::cameraTrs
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___cameraTrs_5;
	// System.Int32 ConGUI::rotSpeed
	int32_t ___rotSpeed_6;
	// UnityEngine.GameObject[] ConGUI::effectObj
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___effectObj_7;
	// UnityEngine.GameObject[] ConGUI::effectObProj
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___effectObProj_8;
	// System.Int32 ConGUI::arrayNo
	int32_t ___arrayNo_9;
	// UnityEngine.GameObject ConGUI::nowEffectObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___nowEffectObj_10;
	// System.String[] ConGUI::cameraState
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___cameraState_11;
	// System.Int32 ConGUI::cameraRotCon
	int32_t ___cameraRotCon_12;
	// System.Single ConGUI::num
	float ___num_13;
	// System.Single ConGUI::numBck
	float ___numBck_14;
	// UnityEngine.Vector3 ConGUI::initPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initPos_15;
	// System.Boolean ConGUI::haveProFlg
	bool ___haveProFlg_16;
	// UnityEngine.GameObject ConGUI::nonProFX
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___nonProFX_17;
	// UnityEngine.Vector3 ConGUI::tmpPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___tmpPos_18;

public:
	inline static int32_t get_offset_of_mainCamera_4() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___mainCamera_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mainCamera_4() const { return ___mainCamera_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mainCamera_4() { return &___mainCamera_4; }
	inline void set_mainCamera_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mainCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_4), value);
	}

	inline static int32_t get_offset_of_cameraTrs_5() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___cameraTrs_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_cameraTrs_5() const { return ___cameraTrs_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_cameraTrs_5() { return &___cameraTrs_5; }
	inline void set_cameraTrs_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___cameraTrs_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTrs_5), value);
	}

	inline static int32_t get_offset_of_rotSpeed_6() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___rotSpeed_6)); }
	inline int32_t get_rotSpeed_6() const { return ___rotSpeed_6; }
	inline int32_t* get_address_of_rotSpeed_6() { return &___rotSpeed_6; }
	inline void set_rotSpeed_6(int32_t value)
	{
		___rotSpeed_6 = value;
	}

	inline static int32_t get_offset_of_effectObj_7() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___effectObj_7)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_effectObj_7() const { return ___effectObj_7; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_effectObj_7() { return &___effectObj_7; }
	inline void set_effectObj_7(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___effectObj_7 = value;
		Il2CppCodeGenWriteBarrier((&___effectObj_7), value);
	}

	inline static int32_t get_offset_of_effectObProj_8() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___effectObProj_8)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_effectObProj_8() const { return ___effectObProj_8; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_effectObProj_8() { return &___effectObProj_8; }
	inline void set_effectObProj_8(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___effectObProj_8 = value;
		Il2CppCodeGenWriteBarrier((&___effectObProj_8), value);
	}

	inline static int32_t get_offset_of_arrayNo_9() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___arrayNo_9)); }
	inline int32_t get_arrayNo_9() const { return ___arrayNo_9; }
	inline int32_t* get_address_of_arrayNo_9() { return &___arrayNo_9; }
	inline void set_arrayNo_9(int32_t value)
	{
		___arrayNo_9 = value;
	}

	inline static int32_t get_offset_of_nowEffectObj_10() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___nowEffectObj_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_nowEffectObj_10() const { return ___nowEffectObj_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_nowEffectObj_10() { return &___nowEffectObj_10; }
	inline void set_nowEffectObj_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___nowEffectObj_10 = value;
		Il2CppCodeGenWriteBarrier((&___nowEffectObj_10), value);
	}

	inline static int32_t get_offset_of_cameraState_11() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___cameraState_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_cameraState_11() const { return ___cameraState_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_cameraState_11() { return &___cameraState_11; }
	inline void set_cameraState_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___cameraState_11 = value;
		Il2CppCodeGenWriteBarrier((&___cameraState_11), value);
	}

	inline static int32_t get_offset_of_cameraRotCon_12() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___cameraRotCon_12)); }
	inline int32_t get_cameraRotCon_12() const { return ___cameraRotCon_12; }
	inline int32_t* get_address_of_cameraRotCon_12() { return &___cameraRotCon_12; }
	inline void set_cameraRotCon_12(int32_t value)
	{
		___cameraRotCon_12 = value;
	}

	inline static int32_t get_offset_of_num_13() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___num_13)); }
	inline float get_num_13() const { return ___num_13; }
	inline float* get_address_of_num_13() { return &___num_13; }
	inline void set_num_13(float value)
	{
		___num_13 = value;
	}

	inline static int32_t get_offset_of_numBck_14() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___numBck_14)); }
	inline float get_numBck_14() const { return ___numBck_14; }
	inline float* get_address_of_numBck_14() { return &___numBck_14; }
	inline void set_numBck_14(float value)
	{
		___numBck_14 = value;
	}

	inline static int32_t get_offset_of_initPos_15() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___initPos_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initPos_15() const { return ___initPos_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initPos_15() { return &___initPos_15; }
	inline void set_initPos_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initPos_15 = value;
	}

	inline static int32_t get_offset_of_haveProFlg_16() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___haveProFlg_16)); }
	inline bool get_haveProFlg_16() const { return ___haveProFlg_16; }
	inline bool* get_address_of_haveProFlg_16() { return &___haveProFlg_16; }
	inline void set_haveProFlg_16(bool value)
	{
		___haveProFlg_16 = value;
	}

	inline static int32_t get_offset_of_nonProFX_17() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___nonProFX_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_nonProFX_17() const { return ___nonProFX_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_nonProFX_17() { return &___nonProFX_17; }
	inline void set_nonProFX_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___nonProFX_17 = value;
		Il2CppCodeGenWriteBarrier((&___nonProFX_17), value);
	}

	inline static int32_t get_offset_of_tmpPos_18() { return static_cast<int32_t>(offsetof(ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84, ___tmpPos_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_tmpPos_18() const { return ___tmpPos_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_tmpPos_18() { return &___tmpPos_18; }
	inline void set_tmpPos_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___tmpPos_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONGUI_TE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84_H
#ifndef DECALDESTROYER_TB4266C3B20B58251EC4488060F2DDBBBFD647F0E_H
#define DECALDESTROYER_TB4266C3B20B58251EC4488060F2DDBBBFD647F0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecalDestroyer
struct  DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single DecalDestroyer::lifeTime
	float ___lifeTime_4;

public:
	inline static int32_t get_offset_of_lifeTime_4() { return static_cast<int32_t>(offsetof(DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E, ___lifeTime_4)); }
	inline float get_lifeTime_4() const { return ___lifeTime_4; }
	inline float* get_address_of_lifeTime_4() { return &___lifeTime_4; }
	inline void set_lifeTime_4(float value)
	{
		___lifeTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECALDESTROYER_TB4266C3B20B58251EC4488060F2DDBBBFD647F0E_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#define DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour/Status DefaultTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_6;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#ifndef EXTINGUISHABLEFIRE_T6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD_H
#define EXTINGUISHABLEFIRE_T6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtinguishableFire
struct  ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem ExtinguishableFire::fireParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___fireParticleSystem_4;
	// UnityEngine.ParticleSystem ExtinguishableFire::smokeParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___smokeParticleSystem_5;
	// System.Boolean ExtinguishableFire::m_isExtinguished
	bool ___m_isExtinguished_6;

public:
	inline static int32_t get_offset_of_fireParticleSystem_4() { return static_cast<int32_t>(offsetof(ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD, ___fireParticleSystem_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_fireParticleSystem_4() const { return ___fireParticleSystem_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_fireParticleSystem_4() { return &___fireParticleSystem_4; }
	inline void set_fireParticleSystem_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___fireParticleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___fireParticleSystem_4), value);
	}

	inline static int32_t get_offset_of_smokeParticleSystem_5() { return static_cast<int32_t>(offsetof(ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD, ___smokeParticleSystem_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_smokeParticleSystem_5() const { return ___smokeParticleSystem_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_smokeParticleSystem_5() { return &___smokeParticleSystem_5; }
	inline void set_smokeParticleSystem_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___smokeParticleSystem_5 = value;
		Il2CppCodeGenWriteBarrier((&___smokeParticleSystem_5), value);
	}

	inline static int32_t get_offset_of_m_isExtinguished_6() { return static_cast<int32_t>(offsetof(ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD, ___m_isExtinguished_6)); }
	inline bool get_m_isExtinguished_6() const { return ___m_isExtinguished_6; }
	inline bool* get_address_of_m_isExtinguished_6() { return &___m_isExtinguished_6; }
	inline void set_m_isExtinguished_6(bool value)
	{
		___m_isExtinguished_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTINGUISHABLEFIRE_T6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD_H
#ifndef FLOATROTATEOBJECT_TFD65C41F499AC9B0AE4C46ABC914D2957297A3F8_H
#define FLOATROTATEOBJECT_TFD65C41F499AC9B0AE4C46ABC914D2957297A3F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatRotateObject
struct  FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single FloatRotateObject::originalY
	float ___originalY_4;
	// System.Boolean FloatRotateObject::rotateEnabled
	bool ___rotateEnabled_5;
	// System.Int16 FloatRotateObject::rotateSpeedX
	int16_t ___rotateSpeedX_6;
	// System.Int16 FloatRotateObject::rotateSpeedY
	int16_t ___rotateSpeedY_7;
	// System.Int16 FloatRotateObject::rotateSpeedZ
	int16_t ___rotateSpeedZ_8;
	// System.Boolean FloatRotateObject::floatEnabled
	bool ___floatEnabled_9;
	// System.Single FloatRotateObject::floatStrength
	float ___floatStrength_10;

public:
	inline static int32_t get_offset_of_originalY_4() { return static_cast<int32_t>(offsetof(FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8, ___originalY_4)); }
	inline float get_originalY_4() const { return ___originalY_4; }
	inline float* get_address_of_originalY_4() { return &___originalY_4; }
	inline void set_originalY_4(float value)
	{
		___originalY_4 = value;
	}

	inline static int32_t get_offset_of_rotateEnabled_5() { return static_cast<int32_t>(offsetof(FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8, ___rotateEnabled_5)); }
	inline bool get_rotateEnabled_5() const { return ___rotateEnabled_5; }
	inline bool* get_address_of_rotateEnabled_5() { return &___rotateEnabled_5; }
	inline void set_rotateEnabled_5(bool value)
	{
		___rotateEnabled_5 = value;
	}

	inline static int32_t get_offset_of_rotateSpeedX_6() { return static_cast<int32_t>(offsetof(FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8, ___rotateSpeedX_6)); }
	inline int16_t get_rotateSpeedX_6() const { return ___rotateSpeedX_6; }
	inline int16_t* get_address_of_rotateSpeedX_6() { return &___rotateSpeedX_6; }
	inline void set_rotateSpeedX_6(int16_t value)
	{
		___rotateSpeedX_6 = value;
	}

	inline static int32_t get_offset_of_rotateSpeedY_7() { return static_cast<int32_t>(offsetof(FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8, ___rotateSpeedY_7)); }
	inline int16_t get_rotateSpeedY_7() const { return ___rotateSpeedY_7; }
	inline int16_t* get_address_of_rotateSpeedY_7() { return &___rotateSpeedY_7; }
	inline void set_rotateSpeedY_7(int16_t value)
	{
		___rotateSpeedY_7 = value;
	}

	inline static int32_t get_offset_of_rotateSpeedZ_8() { return static_cast<int32_t>(offsetof(FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8, ___rotateSpeedZ_8)); }
	inline int16_t get_rotateSpeedZ_8() const { return ___rotateSpeedZ_8; }
	inline int16_t* get_address_of_rotateSpeedZ_8() { return &___rotateSpeedZ_8; }
	inline void set_rotateSpeedZ_8(int16_t value)
	{
		___rotateSpeedZ_8 = value;
	}

	inline static int32_t get_offset_of_floatEnabled_9() { return static_cast<int32_t>(offsetof(FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8, ___floatEnabled_9)); }
	inline bool get_floatEnabled_9() const { return ___floatEnabled_9; }
	inline bool* get_address_of_floatEnabled_9() { return &___floatEnabled_9; }
	inline void set_floatEnabled_9(bool value)
	{
		___floatEnabled_9 = value;
	}

	inline static int32_t get_offset_of_floatStrength_10() { return static_cast<int32_t>(offsetof(FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8, ___floatStrength_10)); }
	inline float get_floatStrength_10() const { return ___floatStrength_10; }
	inline float* get_address_of_floatStrength_10() { return &___floatStrength_10; }
	inline void set_floatStrength_10(float value)
	{
		___floatStrength_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATROTATEOBJECT_TFD65C41F499AC9B0AE4C46ABC914D2957297A3F8_H
#ifndef GUNAIM_TEA32268B21D5E39AD6A0E0C3BF23115E65704880_H
#define GUNAIM_TEA32268B21D5E39AD6A0E0C3BF23115E65704880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GunAim
struct  GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 GunAim::borderLeft
	int32_t ___borderLeft_4;
	// System.Int32 GunAim::borderRight
	int32_t ___borderRight_5;
	// System.Int32 GunAim::borderTop
	int32_t ___borderTop_6;
	// System.Int32 GunAim::borderBottom
	int32_t ___borderBottom_7;
	// UnityEngine.Camera GunAim::parentCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___parentCamera_8;
	// System.Boolean GunAim::isOutOfBounds
	bool ___isOutOfBounds_9;

public:
	inline static int32_t get_offset_of_borderLeft_4() { return static_cast<int32_t>(offsetof(GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880, ___borderLeft_4)); }
	inline int32_t get_borderLeft_4() const { return ___borderLeft_4; }
	inline int32_t* get_address_of_borderLeft_4() { return &___borderLeft_4; }
	inline void set_borderLeft_4(int32_t value)
	{
		___borderLeft_4 = value;
	}

	inline static int32_t get_offset_of_borderRight_5() { return static_cast<int32_t>(offsetof(GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880, ___borderRight_5)); }
	inline int32_t get_borderRight_5() const { return ___borderRight_5; }
	inline int32_t* get_address_of_borderRight_5() { return &___borderRight_5; }
	inline void set_borderRight_5(int32_t value)
	{
		___borderRight_5 = value;
	}

	inline static int32_t get_offset_of_borderTop_6() { return static_cast<int32_t>(offsetof(GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880, ___borderTop_6)); }
	inline int32_t get_borderTop_6() const { return ___borderTop_6; }
	inline int32_t* get_address_of_borderTop_6() { return &___borderTop_6; }
	inline void set_borderTop_6(int32_t value)
	{
		___borderTop_6 = value;
	}

	inline static int32_t get_offset_of_borderBottom_7() { return static_cast<int32_t>(offsetof(GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880, ___borderBottom_7)); }
	inline int32_t get_borderBottom_7() const { return ___borderBottom_7; }
	inline int32_t* get_address_of_borderBottom_7() { return &___borderBottom_7; }
	inline void set_borderBottom_7(int32_t value)
	{
		___borderBottom_7 = value;
	}

	inline static int32_t get_offset_of_parentCamera_8() { return static_cast<int32_t>(offsetof(GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880, ___parentCamera_8)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_parentCamera_8() const { return ___parentCamera_8; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_parentCamera_8() { return &___parentCamera_8; }
	inline void set_parentCamera_8(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___parentCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___parentCamera_8), value);
	}

	inline static int32_t get_offset_of_isOutOfBounds_9() { return static_cast<int32_t>(offsetof(GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880, ___isOutOfBounds_9)); }
	inline bool get_isOutOfBounds_9() const { return ___isOutOfBounds_9; }
	inline bool* get_address_of_isOutOfBounds_9() { return &___isOutOfBounds_9; }
	inline void set_isOutOfBounds_9(bool value)
	{
		___isOutOfBounds_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUNAIM_TEA32268B21D5E39AD6A0E0C3BF23115E65704880_H
#ifndef GUNSHOOT_TEEFCDD0F1E738400870487F8710752C9EC799755_H
#define GUNSHOOT_TEEFCDD0F1E738400870487F8710752C9EC799755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GunShoot
struct  GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single GunShoot::fireRate
	float ___fireRate_4;
	// System.Single GunShoot::weaponRange
	float ___weaponRange_5;
	// UnityEngine.Transform GunShoot::gunEnd
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___gunEnd_6;
	// UnityEngine.ParticleSystem GunShoot::muzzleFlash
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___muzzleFlash_7;
	// UnityEngine.ParticleSystem GunShoot::cartridgeEjection
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___cartridgeEjection_8;
	// UnityEngine.GameObject GunShoot::metalHitEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___metalHitEffect_9;
	// UnityEngine.GameObject GunShoot::sandHitEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___sandHitEffect_10;
	// UnityEngine.GameObject GunShoot::stoneHitEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___stoneHitEffect_11;
	// UnityEngine.GameObject GunShoot::waterLeakEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___waterLeakEffect_12;
	// UnityEngine.GameObject GunShoot::waterLeakExtinguishEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___waterLeakExtinguishEffect_13;
	// UnityEngine.GameObject[] GunShoot::fleshHitEffects
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___fleshHitEffects_14;
	// UnityEngine.GameObject GunShoot::woodHitEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___woodHitEffect_15;
	// System.Single GunShoot::nextFire
	float ___nextFire_16;
	// UnityEngine.Animator GunShoot::anim
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___anim_17;
	// GunAim GunShoot::gunAim
	GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880 * ___gunAim_18;

public:
	inline static int32_t get_offset_of_fireRate_4() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___fireRate_4)); }
	inline float get_fireRate_4() const { return ___fireRate_4; }
	inline float* get_address_of_fireRate_4() { return &___fireRate_4; }
	inline void set_fireRate_4(float value)
	{
		___fireRate_4 = value;
	}

	inline static int32_t get_offset_of_weaponRange_5() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___weaponRange_5)); }
	inline float get_weaponRange_5() const { return ___weaponRange_5; }
	inline float* get_address_of_weaponRange_5() { return &___weaponRange_5; }
	inline void set_weaponRange_5(float value)
	{
		___weaponRange_5 = value;
	}

	inline static int32_t get_offset_of_gunEnd_6() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___gunEnd_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_gunEnd_6() const { return ___gunEnd_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_gunEnd_6() { return &___gunEnd_6; }
	inline void set_gunEnd_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___gunEnd_6 = value;
		Il2CppCodeGenWriteBarrier((&___gunEnd_6), value);
	}

	inline static int32_t get_offset_of_muzzleFlash_7() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___muzzleFlash_7)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_muzzleFlash_7() const { return ___muzzleFlash_7; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_muzzleFlash_7() { return &___muzzleFlash_7; }
	inline void set_muzzleFlash_7(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___muzzleFlash_7 = value;
		Il2CppCodeGenWriteBarrier((&___muzzleFlash_7), value);
	}

	inline static int32_t get_offset_of_cartridgeEjection_8() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___cartridgeEjection_8)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_cartridgeEjection_8() const { return ___cartridgeEjection_8; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_cartridgeEjection_8() { return &___cartridgeEjection_8; }
	inline void set_cartridgeEjection_8(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___cartridgeEjection_8 = value;
		Il2CppCodeGenWriteBarrier((&___cartridgeEjection_8), value);
	}

	inline static int32_t get_offset_of_metalHitEffect_9() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___metalHitEffect_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_metalHitEffect_9() const { return ___metalHitEffect_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_metalHitEffect_9() { return &___metalHitEffect_9; }
	inline void set_metalHitEffect_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___metalHitEffect_9 = value;
		Il2CppCodeGenWriteBarrier((&___metalHitEffect_9), value);
	}

	inline static int32_t get_offset_of_sandHitEffect_10() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___sandHitEffect_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_sandHitEffect_10() const { return ___sandHitEffect_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_sandHitEffect_10() { return &___sandHitEffect_10; }
	inline void set_sandHitEffect_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___sandHitEffect_10 = value;
		Il2CppCodeGenWriteBarrier((&___sandHitEffect_10), value);
	}

	inline static int32_t get_offset_of_stoneHitEffect_11() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___stoneHitEffect_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_stoneHitEffect_11() const { return ___stoneHitEffect_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_stoneHitEffect_11() { return &___stoneHitEffect_11; }
	inline void set_stoneHitEffect_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___stoneHitEffect_11 = value;
		Il2CppCodeGenWriteBarrier((&___stoneHitEffect_11), value);
	}

	inline static int32_t get_offset_of_waterLeakEffect_12() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___waterLeakEffect_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_waterLeakEffect_12() const { return ___waterLeakEffect_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_waterLeakEffect_12() { return &___waterLeakEffect_12; }
	inline void set_waterLeakEffect_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___waterLeakEffect_12 = value;
		Il2CppCodeGenWriteBarrier((&___waterLeakEffect_12), value);
	}

	inline static int32_t get_offset_of_waterLeakExtinguishEffect_13() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___waterLeakExtinguishEffect_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_waterLeakExtinguishEffect_13() const { return ___waterLeakExtinguishEffect_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_waterLeakExtinguishEffect_13() { return &___waterLeakExtinguishEffect_13; }
	inline void set_waterLeakExtinguishEffect_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___waterLeakExtinguishEffect_13 = value;
		Il2CppCodeGenWriteBarrier((&___waterLeakExtinguishEffect_13), value);
	}

	inline static int32_t get_offset_of_fleshHitEffects_14() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___fleshHitEffects_14)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_fleshHitEffects_14() const { return ___fleshHitEffects_14; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_fleshHitEffects_14() { return &___fleshHitEffects_14; }
	inline void set_fleshHitEffects_14(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___fleshHitEffects_14 = value;
		Il2CppCodeGenWriteBarrier((&___fleshHitEffects_14), value);
	}

	inline static int32_t get_offset_of_woodHitEffect_15() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___woodHitEffect_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_woodHitEffect_15() const { return ___woodHitEffect_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_woodHitEffect_15() { return &___woodHitEffect_15; }
	inline void set_woodHitEffect_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___woodHitEffect_15 = value;
		Il2CppCodeGenWriteBarrier((&___woodHitEffect_15), value);
	}

	inline static int32_t get_offset_of_nextFire_16() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___nextFire_16)); }
	inline float get_nextFire_16() const { return ___nextFire_16; }
	inline float* get_address_of_nextFire_16() { return &___nextFire_16; }
	inline void set_nextFire_16(float value)
	{
		___nextFire_16 = value;
	}

	inline static int32_t get_offset_of_anim_17() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___anim_17)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_anim_17() const { return ___anim_17; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_anim_17() { return &___anim_17; }
	inline void set_anim_17(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___anim_17 = value;
		Il2CppCodeGenWriteBarrier((&___anim_17), value);
	}

	inline static int32_t get_offset_of_gunAim_18() { return static_cast<int32_t>(offsetof(GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755, ___gunAim_18)); }
	inline GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880 * get_gunAim_18() const { return ___gunAim_18; }
	inline GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880 ** get_address_of_gunAim_18() { return &___gunAim_18; }
	inline void set_gunAim_18(GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880 * value)
	{
		___gunAim_18 = value;
		Il2CppCodeGenWriteBarrier((&___gunAim_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUNSHOOT_TEEFCDD0F1E738400870487F8710752C9EC799755_H
#ifndef HIDESHOWONTRACK_T93975C6D0DA6560337555A88882A050AD5168D89_H
#define HIDESHOWONTRACK_T93975C6D0DA6560337555A88882A050AD5168D89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HideShowOnTrack
struct  HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HideShowOnTrack::ShowObectsWhenTracked
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___ShowObectsWhenTracked_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> HideShowOnTrack::HideObectsWhenTracked
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___HideObectsWhenTracked_5;
	// System.Boolean HideShowOnTrack::toggleStates
	bool ___toggleStates_6;
	// Vuforia.TrackableBehaviour HideShowOnTrack::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_7;

public:
	inline static int32_t get_offset_of_ShowObectsWhenTracked_4() { return static_cast<int32_t>(offsetof(HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89, ___ShowObectsWhenTracked_4)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_ShowObectsWhenTracked_4() const { return ___ShowObectsWhenTracked_4; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_ShowObectsWhenTracked_4() { return &___ShowObectsWhenTracked_4; }
	inline void set_ShowObectsWhenTracked_4(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___ShowObectsWhenTracked_4 = value;
		Il2CppCodeGenWriteBarrier((&___ShowObectsWhenTracked_4), value);
	}

	inline static int32_t get_offset_of_HideObectsWhenTracked_5() { return static_cast<int32_t>(offsetof(HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89, ___HideObectsWhenTracked_5)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_HideObectsWhenTracked_5() const { return ___HideObectsWhenTracked_5; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_HideObectsWhenTracked_5() { return &___HideObectsWhenTracked_5; }
	inline void set_HideObectsWhenTracked_5(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___HideObectsWhenTracked_5 = value;
		Il2CppCodeGenWriteBarrier((&___HideObectsWhenTracked_5), value);
	}

	inline static int32_t get_offset_of_toggleStates_6() { return static_cast<int32_t>(offsetof(HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89, ___toggleStates_6)); }
	inline bool get_toggleStates_6() const { return ___toggleStates_6; }
	inline bool* get_address_of_toggleStates_6() { return &___toggleStates_6; }
	inline void set_toggleStates_6(bool value)
	{
		___toggleStates_6 = value;
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_7() { return static_cast<int32_t>(offsetof(HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89, ___mTrackableBehaviour_7)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_7() const { return ___mTrackableBehaviour_7; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_7() { return &___mTrackableBehaviour_7; }
	inline void set_mTrackableBehaviour_7(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDESHOWONTRACK_T93975C6D0DA6560337555A88882A050AD5168D89_H
#ifndef LIGHTING_T10410C3F06ACA8870616EF0F564C4FE15E677C37_H
#define LIGHTING_T10410C3F06ACA8870616EF0F564C4FE15E677C37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lighting
struct  Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Lighting::lighting
	float ___lighting_4;
	// UnityEngine.Light Lighting::lightPower
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___lightPower_5;
	// System.Boolean Lighting::flashFlg
	bool ___flashFlg_6;
	// System.Single Lighting::flashTimer
	float ___flashTimer_7;
	// System.Boolean Lighting::lightKeepFlg
	bool ___lightKeepFlg_8;
	// System.Single Lighting::revOnTime
	float ___revOnTime_9;
	// System.Single Lighting::keepOnTime
	float ___keepOnTime_10;
	// System.Single Lighting::keepTime
	float ___keepTime_11;
	// System.Boolean Lighting::flashingFlg
	bool ___flashingFlg_12;
	// System.Single Lighting::minLight
	float ___minLight_13;
	// System.Single Lighting::maxLight
	float ___maxLight_14;
	// System.Boolean Lighting::lightOffFlg
	bool ___lightOffFlg_15;
	// System.Single Lighting::flashingOff
	float ___flashingOff_16;
	// System.Single Lighting::flashingOffPower
	float ___flashingOffPower_17;
	// System.Single Lighting::flashingOffIntensity
	float ___flashingOffIntensity_18;

public:
	inline static int32_t get_offset_of_lighting_4() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___lighting_4)); }
	inline float get_lighting_4() const { return ___lighting_4; }
	inline float* get_address_of_lighting_4() { return &___lighting_4; }
	inline void set_lighting_4(float value)
	{
		___lighting_4 = value;
	}

	inline static int32_t get_offset_of_lightPower_5() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___lightPower_5)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_lightPower_5() const { return ___lightPower_5; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_lightPower_5() { return &___lightPower_5; }
	inline void set_lightPower_5(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___lightPower_5 = value;
		Il2CppCodeGenWriteBarrier((&___lightPower_5), value);
	}

	inline static int32_t get_offset_of_flashFlg_6() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___flashFlg_6)); }
	inline bool get_flashFlg_6() const { return ___flashFlg_6; }
	inline bool* get_address_of_flashFlg_6() { return &___flashFlg_6; }
	inline void set_flashFlg_6(bool value)
	{
		___flashFlg_6 = value;
	}

	inline static int32_t get_offset_of_flashTimer_7() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___flashTimer_7)); }
	inline float get_flashTimer_7() const { return ___flashTimer_7; }
	inline float* get_address_of_flashTimer_7() { return &___flashTimer_7; }
	inline void set_flashTimer_7(float value)
	{
		___flashTimer_7 = value;
	}

	inline static int32_t get_offset_of_lightKeepFlg_8() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___lightKeepFlg_8)); }
	inline bool get_lightKeepFlg_8() const { return ___lightKeepFlg_8; }
	inline bool* get_address_of_lightKeepFlg_8() { return &___lightKeepFlg_8; }
	inline void set_lightKeepFlg_8(bool value)
	{
		___lightKeepFlg_8 = value;
	}

	inline static int32_t get_offset_of_revOnTime_9() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___revOnTime_9)); }
	inline float get_revOnTime_9() const { return ___revOnTime_9; }
	inline float* get_address_of_revOnTime_9() { return &___revOnTime_9; }
	inline void set_revOnTime_9(float value)
	{
		___revOnTime_9 = value;
	}

	inline static int32_t get_offset_of_keepOnTime_10() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___keepOnTime_10)); }
	inline float get_keepOnTime_10() const { return ___keepOnTime_10; }
	inline float* get_address_of_keepOnTime_10() { return &___keepOnTime_10; }
	inline void set_keepOnTime_10(float value)
	{
		___keepOnTime_10 = value;
	}

	inline static int32_t get_offset_of_keepTime_11() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___keepTime_11)); }
	inline float get_keepTime_11() const { return ___keepTime_11; }
	inline float* get_address_of_keepTime_11() { return &___keepTime_11; }
	inline void set_keepTime_11(float value)
	{
		___keepTime_11 = value;
	}

	inline static int32_t get_offset_of_flashingFlg_12() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___flashingFlg_12)); }
	inline bool get_flashingFlg_12() const { return ___flashingFlg_12; }
	inline bool* get_address_of_flashingFlg_12() { return &___flashingFlg_12; }
	inline void set_flashingFlg_12(bool value)
	{
		___flashingFlg_12 = value;
	}

	inline static int32_t get_offset_of_minLight_13() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___minLight_13)); }
	inline float get_minLight_13() const { return ___minLight_13; }
	inline float* get_address_of_minLight_13() { return &___minLight_13; }
	inline void set_minLight_13(float value)
	{
		___minLight_13 = value;
	}

	inline static int32_t get_offset_of_maxLight_14() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___maxLight_14)); }
	inline float get_maxLight_14() const { return ___maxLight_14; }
	inline float* get_address_of_maxLight_14() { return &___maxLight_14; }
	inline void set_maxLight_14(float value)
	{
		___maxLight_14 = value;
	}

	inline static int32_t get_offset_of_lightOffFlg_15() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___lightOffFlg_15)); }
	inline bool get_lightOffFlg_15() const { return ___lightOffFlg_15; }
	inline bool* get_address_of_lightOffFlg_15() { return &___lightOffFlg_15; }
	inline void set_lightOffFlg_15(bool value)
	{
		___lightOffFlg_15 = value;
	}

	inline static int32_t get_offset_of_flashingOff_16() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___flashingOff_16)); }
	inline float get_flashingOff_16() const { return ___flashingOff_16; }
	inline float* get_address_of_flashingOff_16() { return &___flashingOff_16; }
	inline void set_flashingOff_16(float value)
	{
		___flashingOff_16 = value;
	}

	inline static int32_t get_offset_of_flashingOffPower_17() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___flashingOffPower_17)); }
	inline float get_flashingOffPower_17() const { return ___flashingOffPower_17; }
	inline float* get_address_of_flashingOffPower_17() { return &___flashingOffPower_17; }
	inline void set_flashingOffPower_17(float value)
	{
		___flashingOffPower_17 = value;
	}

	inline static int32_t get_offset_of_flashingOffIntensity_18() { return static_cast<int32_t>(offsetof(Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37, ___flashingOffIntensity_18)); }
	inline float get_flashingOffIntensity_18() const { return ___flashingOffIntensity_18; }
	inline float* get_address_of_flashingOffIntensity_18() { return &___flashingOffIntensity_18; }
	inline void set_flashingOffIntensity_18(float value)
	{
		___flashingOffIntensity_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTING_T10410C3F06ACA8870616EF0F564C4FE15E677C37_H
#ifndef PARTICLECOLLISION_TCCB7E309526592B462EDEB34AE317959D704C730_H
#define PARTICLECOLLISION_TCCB7E309526592B462EDEB34AE317959D704C730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleCollision
struct  ParticleCollision_tCCB7E309526592B462EDEB34AE317959D704C730  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.ParticleCollisionEvent> ParticleCollision::m_CollisionEvents
	List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * ___m_CollisionEvents_4;
	// UnityEngine.ParticleSystem ParticleCollision::m_ParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___m_ParticleSystem_5;

public:
	inline static int32_t get_offset_of_m_CollisionEvents_4() { return static_cast<int32_t>(offsetof(ParticleCollision_tCCB7E309526592B462EDEB34AE317959D704C730, ___m_CollisionEvents_4)); }
	inline List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * get_m_CollisionEvents_4() const { return ___m_CollisionEvents_4; }
	inline List_1_t2762C811E470D336E31761384C6E5382164DA4C7 ** get_address_of_m_CollisionEvents_4() { return &___m_CollisionEvents_4; }
	inline void set_m_CollisionEvents_4(List_1_t2762C811E470D336E31761384C6E5382164DA4C7 * value)
	{
		___m_CollisionEvents_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CollisionEvents_4), value);
	}

	inline static int32_t get_offset_of_m_ParticleSystem_5() { return static_cast<int32_t>(offsetof(ParticleCollision_tCCB7E309526592B462EDEB34AE317959D704C730, ___m_ParticleSystem_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_m_ParticleSystem_5() const { return ___m_ParticleSystem_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_m_ParticleSystem_5() { return &___m_ParticleSystem_5; }
	inline void set_m_ParticleSystem_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___m_ParticleSystem_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLECOLLISION_TCCB7E309526592B462EDEB34AE317959D704C730_H
#ifndef PARTICLEMENU_T7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7_H
#define PARTICLEMENU_T7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleMenu
struct  ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ParticleExamples[] ParticleMenu::particleSystems
	ParticleExamplesU5BU5D_t1288EC92287C2C169E23AECAF8E5532A7D003FA6* ___particleSystems_4;
	// UnityEngine.GameObject ParticleMenu::gunGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gunGameObject_5;
	// System.Int32 ParticleMenu::currentIndex
	int32_t ___currentIndex_6;
	// UnityEngine.GameObject ParticleMenu::currentGO
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___currentGO_7;
	// UnityEngine.Transform ParticleMenu::spawnLocation
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___spawnLocation_8;
	// UnityEngine.UI.Text ParticleMenu::title
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___title_9;
	// UnityEngine.UI.Text ParticleMenu::description
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___description_10;
	// UnityEngine.UI.Text ParticleMenu::navigationDetails
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___navigationDetails_11;

public:
	inline static int32_t get_offset_of_particleSystems_4() { return static_cast<int32_t>(offsetof(ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7, ___particleSystems_4)); }
	inline ParticleExamplesU5BU5D_t1288EC92287C2C169E23AECAF8E5532A7D003FA6* get_particleSystems_4() const { return ___particleSystems_4; }
	inline ParticleExamplesU5BU5D_t1288EC92287C2C169E23AECAF8E5532A7D003FA6** get_address_of_particleSystems_4() { return &___particleSystems_4; }
	inline void set_particleSystems_4(ParticleExamplesU5BU5D_t1288EC92287C2C169E23AECAF8E5532A7D003FA6* value)
	{
		___particleSystems_4 = value;
		Il2CppCodeGenWriteBarrier((&___particleSystems_4), value);
	}

	inline static int32_t get_offset_of_gunGameObject_5() { return static_cast<int32_t>(offsetof(ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7, ___gunGameObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gunGameObject_5() const { return ___gunGameObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gunGameObject_5() { return &___gunGameObject_5; }
	inline void set_gunGameObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gunGameObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___gunGameObject_5), value);
	}

	inline static int32_t get_offset_of_currentIndex_6() { return static_cast<int32_t>(offsetof(ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7, ___currentIndex_6)); }
	inline int32_t get_currentIndex_6() const { return ___currentIndex_6; }
	inline int32_t* get_address_of_currentIndex_6() { return &___currentIndex_6; }
	inline void set_currentIndex_6(int32_t value)
	{
		___currentIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentGO_7() { return static_cast<int32_t>(offsetof(ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7, ___currentGO_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_currentGO_7() const { return ___currentGO_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_currentGO_7() { return &___currentGO_7; }
	inline void set_currentGO_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___currentGO_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentGO_7), value);
	}

	inline static int32_t get_offset_of_spawnLocation_8() { return static_cast<int32_t>(offsetof(ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7, ___spawnLocation_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_spawnLocation_8() const { return ___spawnLocation_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_spawnLocation_8() { return &___spawnLocation_8; }
	inline void set_spawnLocation_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___spawnLocation_8 = value;
		Il2CppCodeGenWriteBarrier((&___spawnLocation_8), value);
	}

	inline static int32_t get_offset_of_title_9() { return static_cast<int32_t>(offsetof(ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7, ___title_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_title_9() const { return ___title_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_title_9() { return &___title_9; }
	inline void set_title_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___title_9 = value;
		Il2CppCodeGenWriteBarrier((&___title_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7, ___description_10)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_description_10() const { return ___description_10; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_navigationDetails_11() { return static_cast<int32_t>(offsetof(ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7, ___navigationDetails_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_navigationDetails_11() const { return ___navigationDetails_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_navigationDetails_11() { return &___navigationDetails_11; }
	inline void set_navigationDetails_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___navigationDetails_11 = value;
		Il2CppCodeGenWriteBarrier((&___navigationDetails_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEMENU_T7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7_H
#ifndef PLAYAUDIOCLIPS_T1FCE8227532CD9A81E6383B497AAE71E80F98CE1_H
#define PLAYAUDIOCLIPS_T1FCE8227532CD9A81E6383B497AAE71E80F98CE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayAudioClips
struct  PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> PlayAudioClips::audioClips
	List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * ___audioClips_4;
	// UnityEngine.AudioSource PlayAudioClips::audiosource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audiosource_5;
	// System.Int32 PlayAudioClips::currClip
	int32_t ___currClip_6;
	// Vuforia.TrackableBehaviour PlayAudioClips::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_7;

public:
	inline static int32_t get_offset_of_audioClips_4() { return static_cast<int32_t>(offsetof(PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1, ___audioClips_4)); }
	inline List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * get_audioClips_4() const { return ___audioClips_4; }
	inline List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 ** get_address_of_audioClips_4() { return &___audioClips_4; }
	inline void set_audioClips_4(List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * value)
	{
		___audioClips_4 = value;
		Il2CppCodeGenWriteBarrier((&___audioClips_4), value);
	}

	inline static int32_t get_offset_of_audiosource_5() { return static_cast<int32_t>(offsetof(PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1, ___audiosource_5)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audiosource_5() const { return ___audiosource_5; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audiosource_5() { return &___audiosource_5; }
	inline void set_audiosource_5(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audiosource_5 = value;
		Il2CppCodeGenWriteBarrier((&___audiosource_5), value);
	}

	inline static int32_t get_offset_of_currClip_6() { return static_cast<int32_t>(offsetof(PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1, ___currClip_6)); }
	inline int32_t get_currClip_6() const { return ___currClip_6; }
	inline int32_t* get_address_of_currClip_6() { return &___currClip_6; }
	inline void set_currClip_6(int32_t value)
	{
		___currClip_6 = value;
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_7() { return static_cast<int32_t>(offsetof(PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1, ___mTrackableBehaviour_7)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_7() const { return ___mTrackableBehaviour_7; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_7() { return &___mTrackableBehaviour_7; }
	inline void set_mTrackableBehaviour_7(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYAUDIOCLIPS_T1FCE8227532CD9A81E6383B497AAE71E80F98CE1_H
#ifndef PLAYSOUNDONTRACK_TEAAE33AC01707BB5D53471806DD35217226DB285_H
#define PLAYSOUNDONTRACK_TEAAE33AC01707BB5D53471806DD35217226DB285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaySoundOnTrack
struct  PlaySoundOnTrack_tEAAE33AC01707BB5D53471806DD35217226DB285  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PlaySoundOnTrack::imageTarget
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___imageTarget_4;
	// UnityEngine.AudioSource PlaySoundOnTrack::audio
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audio_5;
	// Vuforia.TrackableBehaviour PlaySoundOnTrack::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_6;

public:
	inline static int32_t get_offset_of_imageTarget_4() { return static_cast<int32_t>(offsetof(PlaySoundOnTrack_tEAAE33AC01707BB5D53471806DD35217226DB285, ___imageTarget_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_imageTarget_4() const { return ___imageTarget_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_imageTarget_4() { return &___imageTarget_4; }
	inline void set_imageTarget_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___imageTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___imageTarget_4), value);
	}

	inline static int32_t get_offset_of_audio_5() { return static_cast<int32_t>(offsetof(PlaySoundOnTrack_tEAAE33AC01707BB5D53471806DD35217226DB285, ___audio_5)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audio_5() const { return ___audio_5; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audio_5() { return &___audio_5; }
	inline void set_audio_5(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audio_5 = value;
		Il2CppCodeGenWriteBarrier((&___audio_5), value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_6() { return static_cast<int32_t>(offsetof(PlaySoundOnTrack_tEAAE33AC01707BB5D53471806DD35217226DB285, ___mTrackableBehaviour_6)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_6() const { return ___mTrackableBehaviour_6; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_6() { return &___mTrackableBehaviour_6; }
	inline void set_mTrackableBehaviour_6(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYSOUNDONTRACK_TEAAE33AC01707BB5D53471806DD35217226DB285_H
#ifndef STARTPARTICLESONTRACKING_T83CB5FC0A4988B440DAA2430485F059135EA98CC_H
#define STARTPARTICLESONTRACKING_T83CB5FC0A4988B440DAA2430485F059135EA98CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartParticlesOnTracking
struct  StartParticlesOnTracking_t83CB5FC0A4988B440DAA2430485F059135EA98CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject StartParticlesOnTracking::trackableObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___trackableObject_4;
	// Vuforia.TrackableBehaviour StartParticlesOnTracking::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_5;
	// UnityEngine.ParticleSystem StartParticlesOnTracking::particles
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___particles_6;

public:
	inline static int32_t get_offset_of_trackableObject_4() { return static_cast<int32_t>(offsetof(StartParticlesOnTracking_t83CB5FC0A4988B440DAA2430485F059135EA98CC, ___trackableObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_trackableObject_4() const { return ___trackableObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_trackableObject_4() { return &___trackableObject_4; }
	inline void set_trackableObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___trackableObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___trackableObject_4), value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_5() { return static_cast<int32_t>(offsetof(StartParticlesOnTracking_t83CB5FC0A4988B440DAA2430485F059135EA98CC, ___mTrackableBehaviour_5)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_5() const { return ___mTrackableBehaviour_5; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_5() { return &___mTrackableBehaviour_5; }
	inline void set_mTrackableBehaviour_5(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_5), value);
	}

	inline static int32_t get_offset_of_particles_6() { return static_cast<int32_t>(offsetof(StartParticlesOnTracking_t83CB5FC0A4988B440DAA2430485F059135EA98CC, ___particles_6)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_particles_6() const { return ___particles_6; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_particles_6() { return &___particles_6; }
	inline void set_particles_6(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___particles_6 = value;
		Il2CppCodeGenWriteBarrier((&___particles_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTPARTICLESONTRACKING_T83CB5FC0A4988B440DAA2430485F059135EA98CC_H
#ifndef VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#define VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifndef WFX_BULLETHOLEDECAL_T75BB8417732D09D9092DE9D8A3A30B3EABE376F5_H
#define WFX_BULLETHOLEDECAL_T75BB8417732D09D9092DE9D8A3A30B3EABE376F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_BulletHoleDecal
struct  WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single WFX_BulletHoleDecal::lifetime
	float ___lifetime_5;
	// System.Single WFX_BulletHoleDecal::fadeoutpercent
	float ___fadeoutpercent_6;
	// UnityEngine.Vector2 WFX_BulletHoleDecal::frames
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___frames_7;
	// System.Boolean WFX_BulletHoleDecal::randomRotation
	bool ___randomRotation_8;
	// System.Boolean WFX_BulletHoleDecal::deactivate
	bool ___deactivate_9;
	// System.Single WFX_BulletHoleDecal::life
	float ___life_10;
	// System.Single WFX_BulletHoleDecal::fadeout
	float ___fadeout_11;
	// UnityEngine.Color WFX_BulletHoleDecal::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_12;
	// System.Single WFX_BulletHoleDecal::orgAlpha
	float ___orgAlpha_13;

public:
	inline static int32_t get_offset_of_lifetime_5() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___lifetime_5)); }
	inline float get_lifetime_5() const { return ___lifetime_5; }
	inline float* get_address_of_lifetime_5() { return &___lifetime_5; }
	inline void set_lifetime_5(float value)
	{
		___lifetime_5 = value;
	}

	inline static int32_t get_offset_of_fadeoutpercent_6() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___fadeoutpercent_6)); }
	inline float get_fadeoutpercent_6() const { return ___fadeoutpercent_6; }
	inline float* get_address_of_fadeoutpercent_6() { return &___fadeoutpercent_6; }
	inline void set_fadeoutpercent_6(float value)
	{
		___fadeoutpercent_6 = value;
	}

	inline static int32_t get_offset_of_frames_7() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___frames_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_frames_7() const { return ___frames_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_frames_7() { return &___frames_7; }
	inline void set_frames_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___frames_7 = value;
	}

	inline static int32_t get_offset_of_randomRotation_8() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___randomRotation_8)); }
	inline bool get_randomRotation_8() const { return ___randomRotation_8; }
	inline bool* get_address_of_randomRotation_8() { return &___randomRotation_8; }
	inline void set_randomRotation_8(bool value)
	{
		___randomRotation_8 = value;
	}

	inline static int32_t get_offset_of_deactivate_9() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___deactivate_9)); }
	inline bool get_deactivate_9() const { return ___deactivate_9; }
	inline bool* get_address_of_deactivate_9() { return &___deactivate_9; }
	inline void set_deactivate_9(bool value)
	{
		___deactivate_9 = value;
	}

	inline static int32_t get_offset_of_life_10() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___life_10)); }
	inline float get_life_10() const { return ___life_10; }
	inline float* get_address_of_life_10() { return &___life_10; }
	inline void set_life_10(float value)
	{
		___life_10 = value;
	}

	inline static int32_t get_offset_of_fadeout_11() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___fadeout_11)); }
	inline float get_fadeout_11() const { return ___fadeout_11; }
	inline float* get_address_of_fadeout_11() { return &___fadeout_11; }
	inline void set_fadeout_11(float value)
	{
		___fadeout_11 = value;
	}

	inline static int32_t get_offset_of_color_12() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___color_12)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_12() const { return ___color_12; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_12() { return &___color_12; }
	inline void set_color_12(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_12 = value;
	}

	inline static int32_t get_offset_of_orgAlpha_13() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5, ___orgAlpha_13)); }
	inline float get_orgAlpha_13() const { return ___orgAlpha_13; }
	inline float* get_address_of_orgAlpha_13() { return &___orgAlpha_13; }
	inline void set_orgAlpha_13(float value)
	{
		___orgAlpha_13 = value;
	}
};

struct WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5_StaticFields
{
public:
	// UnityEngine.Vector2[] WFX_BulletHoleDecal::quadUVs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___quadUVs_4;

public:
	inline static int32_t get_offset_of_quadUVs_4() { return static_cast<int32_t>(offsetof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5_StaticFields, ___quadUVs_4)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_quadUVs_4() const { return ___quadUVs_4; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_quadUVs_4() { return &___quadUVs_4; }
	inline void set_quadUVs_4(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___quadUVs_4 = value;
		Il2CppCodeGenWriteBarrier((&___quadUVs_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WFX_BULLETHOLEDECAL_T75BB8417732D09D9092DE9D8A3A30B3EABE376F5_H
#ifndef WFX_DEMO_T45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E_H
#define WFX_DEMO_T45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_Demo
struct  WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single WFX_Demo::cameraSpeed
	float ___cameraSpeed_4;
	// System.Boolean WFX_Demo::orderedSpawns
	bool ___orderedSpawns_5;
	// System.Single WFX_Demo::step
	float ___step_6;
	// System.Single WFX_Demo::range
	float ___range_7;
	// System.Single WFX_Demo::order
	float ___order_8;
	// UnityEngine.GameObject WFX_Demo::walls
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___walls_9;
	// UnityEngine.GameObject WFX_Demo::bulletholes
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bulletholes_10;
	// UnityEngine.GameObject[] WFX_Demo::ParticleExamples
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___ParticleExamples_11;
	// System.Int32 WFX_Demo::exampleIndex
	int32_t ___exampleIndex_12;
	// System.String WFX_Demo::randomSpawnsDelay
	String_t* ___randomSpawnsDelay_13;
	// System.Boolean WFX_Demo::randomSpawns
	bool ___randomSpawns_14;
	// System.Boolean WFX_Demo::slowMo
	bool ___slowMo_15;
	// System.Boolean WFX_Demo::rotateCam
	bool ___rotateCam_16;
	// UnityEngine.Material WFX_Demo::wood
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___wood_17;
	// UnityEngine.Material WFX_Demo::concrete
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___concrete_18;
	// UnityEngine.Material WFX_Demo::metal
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___metal_19;
	// UnityEngine.Material WFX_Demo::checker
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___checker_20;
	// UnityEngine.Material WFX_Demo::woodWall
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___woodWall_21;
	// UnityEngine.Material WFX_Demo::concreteWall
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___concreteWall_22;
	// UnityEngine.Material WFX_Demo::metalWall
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___metalWall_23;
	// UnityEngine.Material WFX_Demo::checkerWall
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___checkerWall_24;
	// System.String WFX_Demo::groundTextureStr
	String_t* ___groundTextureStr_25;
	// System.Collections.Generic.List`1<System.String> WFX_Demo::groundTextures
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___groundTextures_26;
	// UnityEngine.GameObject WFX_Demo::m4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m4_27;
	// UnityEngine.GameObject WFX_Demo::m4fps
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m4fps_28;
	// System.Boolean WFX_Demo::rotate_m4
	bool ___rotate_m4_29;

public:
	inline static int32_t get_offset_of_cameraSpeed_4() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___cameraSpeed_4)); }
	inline float get_cameraSpeed_4() const { return ___cameraSpeed_4; }
	inline float* get_address_of_cameraSpeed_4() { return &___cameraSpeed_4; }
	inline void set_cameraSpeed_4(float value)
	{
		___cameraSpeed_4 = value;
	}

	inline static int32_t get_offset_of_orderedSpawns_5() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___orderedSpawns_5)); }
	inline bool get_orderedSpawns_5() const { return ___orderedSpawns_5; }
	inline bool* get_address_of_orderedSpawns_5() { return &___orderedSpawns_5; }
	inline void set_orderedSpawns_5(bool value)
	{
		___orderedSpawns_5 = value;
	}

	inline static int32_t get_offset_of_step_6() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___step_6)); }
	inline float get_step_6() const { return ___step_6; }
	inline float* get_address_of_step_6() { return &___step_6; }
	inline void set_step_6(float value)
	{
		___step_6 = value;
	}

	inline static int32_t get_offset_of_range_7() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___range_7)); }
	inline float get_range_7() const { return ___range_7; }
	inline float* get_address_of_range_7() { return &___range_7; }
	inline void set_range_7(float value)
	{
		___range_7 = value;
	}

	inline static int32_t get_offset_of_order_8() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___order_8)); }
	inline float get_order_8() const { return ___order_8; }
	inline float* get_address_of_order_8() { return &___order_8; }
	inline void set_order_8(float value)
	{
		___order_8 = value;
	}

	inline static int32_t get_offset_of_walls_9() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___walls_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_walls_9() const { return ___walls_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_walls_9() { return &___walls_9; }
	inline void set_walls_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___walls_9 = value;
		Il2CppCodeGenWriteBarrier((&___walls_9), value);
	}

	inline static int32_t get_offset_of_bulletholes_10() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___bulletholes_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bulletholes_10() const { return ___bulletholes_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bulletholes_10() { return &___bulletholes_10; }
	inline void set_bulletholes_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bulletholes_10 = value;
		Il2CppCodeGenWriteBarrier((&___bulletholes_10), value);
	}

	inline static int32_t get_offset_of_ParticleExamples_11() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___ParticleExamples_11)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_ParticleExamples_11() const { return ___ParticleExamples_11; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_ParticleExamples_11() { return &___ParticleExamples_11; }
	inline void set_ParticleExamples_11(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___ParticleExamples_11 = value;
		Il2CppCodeGenWriteBarrier((&___ParticleExamples_11), value);
	}

	inline static int32_t get_offset_of_exampleIndex_12() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___exampleIndex_12)); }
	inline int32_t get_exampleIndex_12() const { return ___exampleIndex_12; }
	inline int32_t* get_address_of_exampleIndex_12() { return &___exampleIndex_12; }
	inline void set_exampleIndex_12(int32_t value)
	{
		___exampleIndex_12 = value;
	}

	inline static int32_t get_offset_of_randomSpawnsDelay_13() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___randomSpawnsDelay_13)); }
	inline String_t* get_randomSpawnsDelay_13() const { return ___randomSpawnsDelay_13; }
	inline String_t** get_address_of_randomSpawnsDelay_13() { return &___randomSpawnsDelay_13; }
	inline void set_randomSpawnsDelay_13(String_t* value)
	{
		___randomSpawnsDelay_13 = value;
		Il2CppCodeGenWriteBarrier((&___randomSpawnsDelay_13), value);
	}

	inline static int32_t get_offset_of_randomSpawns_14() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___randomSpawns_14)); }
	inline bool get_randomSpawns_14() const { return ___randomSpawns_14; }
	inline bool* get_address_of_randomSpawns_14() { return &___randomSpawns_14; }
	inline void set_randomSpawns_14(bool value)
	{
		___randomSpawns_14 = value;
	}

	inline static int32_t get_offset_of_slowMo_15() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___slowMo_15)); }
	inline bool get_slowMo_15() const { return ___slowMo_15; }
	inline bool* get_address_of_slowMo_15() { return &___slowMo_15; }
	inline void set_slowMo_15(bool value)
	{
		___slowMo_15 = value;
	}

	inline static int32_t get_offset_of_rotateCam_16() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___rotateCam_16)); }
	inline bool get_rotateCam_16() const { return ___rotateCam_16; }
	inline bool* get_address_of_rotateCam_16() { return &___rotateCam_16; }
	inline void set_rotateCam_16(bool value)
	{
		___rotateCam_16 = value;
	}

	inline static int32_t get_offset_of_wood_17() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___wood_17)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_wood_17() const { return ___wood_17; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_wood_17() { return &___wood_17; }
	inline void set_wood_17(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___wood_17 = value;
		Il2CppCodeGenWriteBarrier((&___wood_17), value);
	}

	inline static int32_t get_offset_of_concrete_18() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___concrete_18)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_concrete_18() const { return ___concrete_18; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_concrete_18() { return &___concrete_18; }
	inline void set_concrete_18(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___concrete_18 = value;
		Il2CppCodeGenWriteBarrier((&___concrete_18), value);
	}

	inline static int32_t get_offset_of_metal_19() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___metal_19)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_metal_19() const { return ___metal_19; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_metal_19() { return &___metal_19; }
	inline void set_metal_19(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___metal_19 = value;
		Il2CppCodeGenWriteBarrier((&___metal_19), value);
	}

	inline static int32_t get_offset_of_checker_20() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___checker_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_checker_20() const { return ___checker_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_checker_20() { return &___checker_20; }
	inline void set_checker_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___checker_20 = value;
		Il2CppCodeGenWriteBarrier((&___checker_20), value);
	}

	inline static int32_t get_offset_of_woodWall_21() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___woodWall_21)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_woodWall_21() const { return ___woodWall_21; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_woodWall_21() { return &___woodWall_21; }
	inline void set_woodWall_21(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___woodWall_21 = value;
		Il2CppCodeGenWriteBarrier((&___woodWall_21), value);
	}

	inline static int32_t get_offset_of_concreteWall_22() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___concreteWall_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_concreteWall_22() const { return ___concreteWall_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_concreteWall_22() { return &___concreteWall_22; }
	inline void set_concreteWall_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___concreteWall_22 = value;
		Il2CppCodeGenWriteBarrier((&___concreteWall_22), value);
	}

	inline static int32_t get_offset_of_metalWall_23() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___metalWall_23)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_metalWall_23() const { return ___metalWall_23; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_metalWall_23() { return &___metalWall_23; }
	inline void set_metalWall_23(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___metalWall_23 = value;
		Il2CppCodeGenWriteBarrier((&___metalWall_23), value);
	}

	inline static int32_t get_offset_of_checkerWall_24() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___checkerWall_24)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_checkerWall_24() const { return ___checkerWall_24; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_checkerWall_24() { return &___checkerWall_24; }
	inline void set_checkerWall_24(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___checkerWall_24 = value;
		Il2CppCodeGenWriteBarrier((&___checkerWall_24), value);
	}

	inline static int32_t get_offset_of_groundTextureStr_25() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___groundTextureStr_25)); }
	inline String_t* get_groundTextureStr_25() const { return ___groundTextureStr_25; }
	inline String_t** get_address_of_groundTextureStr_25() { return &___groundTextureStr_25; }
	inline void set_groundTextureStr_25(String_t* value)
	{
		___groundTextureStr_25 = value;
		Il2CppCodeGenWriteBarrier((&___groundTextureStr_25), value);
	}

	inline static int32_t get_offset_of_groundTextures_26() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___groundTextures_26)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_groundTextures_26() const { return ___groundTextures_26; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_groundTextures_26() { return &___groundTextures_26; }
	inline void set_groundTextures_26(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___groundTextures_26 = value;
		Il2CppCodeGenWriteBarrier((&___groundTextures_26), value);
	}

	inline static int32_t get_offset_of_m4_27() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___m4_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m4_27() const { return ___m4_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m4_27() { return &___m4_27; }
	inline void set_m4_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m4_27 = value;
		Il2CppCodeGenWriteBarrier((&___m4_27), value);
	}

	inline static int32_t get_offset_of_m4fps_28() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___m4fps_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m4fps_28() const { return ___m4fps_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m4fps_28() { return &___m4fps_28; }
	inline void set_m4fps_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m4fps_28 = value;
		Il2CppCodeGenWriteBarrier((&___m4fps_28), value);
	}

	inline static int32_t get_offset_of_rotate_m4_29() { return static_cast<int32_t>(offsetof(WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E, ___rotate_m4_29)); }
	inline bool get_rotate_m4_29() const { return ___rotate_m4_29; }
	inline bool* get_address_of_rotate_m4_29() { return &___rotate_m4_29; }
	inline void set_rotate_m4_29(bool value)
	{
		___rotate_m4_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WFX_DEMO_T45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E_H
#ifndef WFX_DEMO_DELETEAFTERDELAY_TA71701362CD4B07A41161C3BFCE2B4882E2DC2EB_H
#define WFX_DEMO_DELETEAFTERDELAY_TA71701362CD4B07A41161C3BFCE2B4882E2DC2EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_Demo_DeleteAfterDelay
struct  WFX_Demo_DeleteAfterDelay_tA71701362CD4B07A41161C3BFCE2B4882E2DC2EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single WFX_Demo_DeleteAfterDelay::delay
	float ___delay_4;

public:
	inline static int32_t get_offset_of_delay_4() { return static_cast<int32_t>(offsetof(WFX_Demo_DeleteAfterDelay_tA71701362CD4B07A41161C3BFCE2B4882E2DC2EB, ___delay_4)); }
	inline float get_delay_4() const { return ___delay_4; }
	inline float* get_address_of_delay_4() { return &___delay_4; }
	inline void set_delay_4(float value)
	{
		___delay_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WFX_DEMO_DELETEAFTERDELAY_TA71701362CD4B07A41161C3BFCE2B4882E2DC2EB_H
#ifndef WFX_DEMO_NEW_TEB509364A67554E8243EDD6A554AEA30D860F1AB_H
#define WFX_DEMO_NEW_TEB509364A67554E8243EDD6A554AEA30D860F1AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_Demo_New
struct  WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Renderer WFX_Demo_New::groundRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___groundRenderer_4;
	// UnityEngine.Collider WFX_Demo_New::groundCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___groundCollider_5;
	// UnityEngine.UI.Image WFX_Demo_New::slowMoBtn
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___slowMoBtn_6;
	// UnityEngine.UI.Text WFX_Demo_New::slowMoLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___slowMoLabel_7;
	// UnityEngine.UI.Image WFX_Demo_New::camRotBtn
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___camRotBtn_8;
	// UnityEngine.UI.Text WFX_Demo_New::camRotLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___camRotLabel_9;
	// UnityEngine.UI.Image WFX_Demo_New::groundBtn
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___groundBtn_10;
	// UnityEngine.UI.Text WFX_Demo_New::groundLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___groundLabel_11;
	// UnityEngine.UI.Text WFX_Demo_New::EffectLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___EffectLabel_12;
	// UnityEngine.UI.Text WFX_Demo_New::EffectIndexLabel
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___EffectIndexLabel_13;
	// UnityEngine.GameObject[] WFX_Demo_New::AdditionalEffects
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___AdditionalEffects_14;
	// UnityEngine.GameObject WFX_Demo_New::ground
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ground_15;
	// UnityEngine.GameObject WFX_Demo_New::walls
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___walls_16;
	// UnityEngine.GameObject WFX_Demo_New::bulletholes
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bulletholes_17;
	// UnityEngine.GameObject WFX_Demo_New::m4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m4_18;
	// UnityEngine.GameObject WFX_Demo_New::m4fps
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m4fps_19;
	// UnityEngine.Material WFX_Demo_New::wood
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___wood_20;
	// UnityEngine.Material WFX_Demo_New::concrete
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___concrete_21;
	// UnityEngine.Material WFX_Demo_New::metal
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___metal_22;
	// UnityEngine.Material WFX_Demo_New::checker
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___checker_23;
	// UnityEngine.Material WFX_Demo_New::woodWall
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___woodWall_24;
	// UnityEngine.Material WFX_Demo_New::concreteWall
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___concreteWall_25;
	// UnityEngine.Material WFX_Demo_New::metalWall
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___metalWall_26;
	// UnityEngine.Material WFX_Demo_New::checkerWall
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___checkerWall_27;
	// System.String WFX_Demo_New::groundTextureStr
	String_t* ___groundTextureStr_28;
	// System.Collections.Generic.List`1<System.String> WFX_Demo_New::groundTextures
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___groundTextures_29;
	// UnityEngine.GameObject[] WFX_Demo_New::ParticleExamples
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___ParticleExamples_30;
	// System.Int32 WFX_Demo_New::exampleIndex
	int32_t ___exampleIndex_31;
	// System.Boolean WFX_Demo_New::slowMo
	bool ___slowMo_32;
	// UnityEngine.Vector3 WFX_Demo_New::defaultCamPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___defaultCamPosition_33;
	// UnityEngine.Quaternion WFX_Demo_New::defaultCamRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___defaultCamRotation_34;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> WFX_Demo_New::onScreenParticles
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___onScreenParticles_35;

public:
	inline static int32_t get_offset_of_groundRenderer_4() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___groundRenderer_4)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_groundRenderer_4() const { return ___groundRenderer_4; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_groundRenderer_4() { return &___groundRenderer_4; }
	inline void set_groundRenderer_4(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___groundRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___groundRenderer_4), value);
	}

	inline static int32_t get_offset_of_groundCollider_5() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___groundCollider_5)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_groundCollider_5() const { return ___groundCollider_5; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_groundCollider_5() { return &___groundCollider_5; }
	inline void set_groundCollider_5(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___groundCollider_5 = value;
		Il2CppCodeGenWriteBarrier((&___groundCollider_5), value);
	}

	inline static int32_t get_offset_of_slowMoBtn_6() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___slowMoBtn_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_slowMoBtn_6() const { return ___slowMoBtn_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_slowMoBtn_6() { return &___slowMoBtn_6; }
	inline void set_slowMoBtn_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___slowMoBtn_6 = value;
		Il2CppCodeGenWriteBarrier((&___slowMoBtn_6), value);
	}

	inline static int32_t get_offset_of_slowMoLabel_7() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___slowMoLabel_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_slowMoLabel_7() const { return ___slowMoLabel_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_slowMoLabel_7() { return &___slowMoLabel_7; }
	inline void set_slowMoLabel_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___slowMoLabel_7 = value;
		Il2CppCodeGenWriteBarrier((&___slowMoLabel_7), value);
	}

	inline static int32_t get_offset_of_camRotBtn_8() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___camRotBtn_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_camRotBtn_8() const { return ___camRotBtn_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_camRotBtn_8() { return &___camRotBtn_8; }
	inline void set_camRotBtn_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___camRotBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&___camRotBtn_8), value);
	}

	inline static int32_t get_offset_of_camRotLabel_9() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___camRotLabel_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_camRotLabel_9() const { return ___camRotLabel_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_camRotLabel_9() { return &___camRotLabel_9; }
	inline void set_camRotLabel_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___camRotLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&___camRotLabel_9), value);
	}

	inline static int32_t get_offset_of_groundBtn_10() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___groundBtn_10)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_groundBtn_10() const { return ___groundBtn_10; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_groundBtn_10() { return &___groundBtn_10; }
	inline void set_groundBtn_10(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___groundBtn_10 = value;
		Il2CppCodeGenWriteBarrier((&___groundBtn_10), value);
	}

	inline static int32_t get_offset_of_groundLabel_11() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___groundLabel_11)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_groundLabel_11() const { return ___groundLabel_11; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_groundLabel_11() { return &___groundLabel_11; }
	inline void set_groundLabel_11(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___groundLabel_11 = value;
		Il2CppCodeGenWriteBarrier((&___groundLabel_11), value);
	}

	inline static int32_t get_offset_of_EffectLabel_12() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___EffectLabel_12)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_EffectLabel_12() const { return ___EffectLabel_12; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_EffectLabel_12() { return &___EffectLabel_12; }
	inline void set_EffectLabel_12(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___EffectLabel_12 = value;
		Il2CppCodeGenWriteBarrier((&___EffectLabel_12), value);
	}

	inline static int32_t get_offset_of_EffectIndexLabel_13() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___EffectIndexLabel_13)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_EffectIndexLabel_13() const { return ___EffectIndexLabel_13; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_EffectIndexLabel_13() { return &___EffectIndexLabel_13; }
	inline void set_EffectIndexLabel_13(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___EffectIndexLabel_13 = value;
		Il2CppCodeGenWriteBarrier((&___EffectIndexLabel_13), value);
	}

	inline static int32_t get_offset_of_AdditionalEffects_14() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___AdditionalEffects_14)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_AdditionalEffects_14() const { return ___AdditionalEffects_14; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_AdditionalEffects_14() { return &___AdditionalEffects_14; }
	inline void set_AdditionalEffects_14(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___AdditionalEffects_14 = value;
		Il2CppCodeGenWriteBarrier((&___AdditionalEffects_14), value);
	}

	inline static int32_t get_offset_of_ground_15() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___ground_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ground_15() const { return ___ground_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ground_15() { return &___ground_15; }
	inline void set_ground_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ground_15 = value;
		Il2CppCodeGenWriteBarrier((&___ground_15), value);
	}

	inline static int32_t get_offset_of_walls_16() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___walls_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_walls_16() const { return ___walls_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_walls_16() { return &___walls_16; }
	inline void set_walls_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___walls_16 = value;
		Il2CppCodeGenWriteBarrier((&___walls_16), value);
	}

	inline static int32_t get_offset_of_bulletholes_17() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___bulletholes_17)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bulletholes_17() const { return ___bulletholes_17; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bulletholes_17() { return &___bulletholes_17; }
	inline void set_bulletholes_17(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bulletholes_17 = value;
		Il2CppCodeGenWriteBarrier((&___bulletholes_17), value);
	}

	inline static int32_t get_offset_of_m4_18() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___m4_18)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m4_18() const { return ___m4_18; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m4_18() { return &___m4_18; }
	inline void set_m4_18(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m4_18 = value;
		Il2CppCodeGenWriteBarrier((&___m4_18), value);
	}

	inline static int32_t get_offset_of_m4fps_19() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___m4fps_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m4fps_19() const { return ___m4fps_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m4fps_19() { return &___m4fps_19; }
	inline void set_m4fps_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m4fps_19 = value;
		Il2CppCodeGenWriteBarrier((&___m4fps_19), value);
	}

	inline static int32_t get_offset_of_wood_20() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___wood_20)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_wood_20() const { return ___wood_20; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_wood_20() { return &___wood_20; }
	inline void set_wood_20(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___wood_20 = value;
		Il2CppCodeGenWriteBarrier((&___wood_20), value);
	}

	inline static int32_t get_offset_of_concrete_21() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___concrete_21)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_concrete_21() const { return ___concrete_21; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_concrete_21() { return &___concrete_21; }
	inline void set_concrete_21(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___concrete_21 = value;
		Il2CppCodeGenWriteBarrier((&___concrete_21), value);
	}

	inline static int32_t get_offset_of_metal_22() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___metal_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_metal_22() const { return ___metal_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_metal_22() { return &___metal_22; }
	inline void set_metal_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___metal_22 = value;
		Il2CppCodeGenWriteBarrier((&___metal_22), value);
	}

	inline static int32_t get_offset_of_checker_23() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___checker_23)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_checker_23() const { return ___checker_23; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_checker_23() { return &___checker_23; }
	inline void set_checker_23(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___checker_23 = value;
		Il2CppCodeGenWriteBarrier((&___checker_23), value);
	}

	inline static int32_t get_offset_of_woodWall_24() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___woodWall_24)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_woodWall_24() const { return ___woodWall_24; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_woodWall_24() { return &___woodWall_24; }
	inline void set_woodWall_24(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___woodWall_24 = value;
		Il2CppCodeGenWriteBarrier((&___woodWall_24), value);
	}

	inline static int32_t get_offset_of_concreteWall_25() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___concreteWall_25)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_concreteWall_25() const { return ___concreteWall_25; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_concreteWall_25() { return &___concreteWall_25; }
	inline void set_concreteWall_25(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___concreteWall_25 = value;
		Il2CppCodeGenWriteBarrier((&___concreteWall_25), value);
	}

	inline static int32_t get_offset_of_metalWall_26() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___metalWall_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_metalWall_26() const { return ___metalWall_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_metalWall_26() { return &___metalWall_26; }
	inline void set_metalWall_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___metalWall_26 = value;
		Il2CppCodeGenWriteBarrier((&___metalWall_26), value);
	}

	inline static int32_t get_offset_of_checkerWall_27() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___checkerWall_27)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_checkerWall_27() const { return ___checkerWall_27; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_checkerWall_27() { return &___checkerWall_27; }
	inline void set_checkerWall_27(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___checkerWall_27 = value;
		Il2CppCodeGenWriteBarrier((&___checkerWall_27), value);
	}

	inline static int32_t get_offset_of_groundTextureStr_28() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___groundTextureStr_28)); }
	inline String_t* get_groundTextureStr_28() const { return ___groundTextureStr_28; }
	inline String_t** get_address_of_groundTextureStr_28() { return &___groundTextureStr_28; }
	inline void set_groundTextureStr_28(String_t* value)
	{
		___groundTextureStr_28 = value;
		Il2CppCodeGenWriteBarrier((&___groundTextureStr_28), value);
	}

	inline static int32_t get_offset_of_groundTextures_29() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___groundTextures_29)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_groundTextures_29() const { return ___groundTextures_29; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_groundTextures_29() { return &___groundTextures_29; }
	inline void set_groundTextures_29(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___groundTextures_29 = value;
		Il2CppCodeGenWriteBarrier((&___groundTextures_29), value);
	}

	inline static int32_t get_offset_of_ParticleExamples_30() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___ParticleExamples_30)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_ParticleExamples_30() const { return ___ParticleExamples_30; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_ParticleExamples_30() { return &___ParticleExamples_30; }
	inline void set_ParticleExamples_30(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___ParticleExamples_30 = value;
		Il2CppCodeGenWriteBarrier((&___ParticleExamples_30), value);
	}

	inline static int32_t get_offset_of_exampleIndex_31() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___exampleIndex_31)); }
	inline int32_t get_exampleIndex_31() const { return ___exampleIndex_31; }
	inline int32_t* get_address_of_exampleIndex_31() { return &___exampleIndex_31; }
	inline void set_exampleIndex_31(int32_t value)
	{
		___exampleIndex_31 = value;
	}

	inline static int32_t get_offset_of_slowMo_32() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___slowMo_32)); }
	inline bool get_slowMo_32() const { return ___slowMo_32; }
	inline bool* get_address_of_slowMo_32() { return &___slowMo_32; }
	inline void set_slowMo_32(bool value)
	{
		___slowMo_32 = value;
	}

	inline static int32_t get_offset_of_defaultCamPosition_33() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___defaultCamPosition_33)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_defaultCamPosition_33() const { return ___defaultCamPosition_33; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_defaultCamPosition_33() { return &___defaultCamPosition_33; }
	inline void set_defaultCamPosition_33(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___defaultCamPosition_33 = value;
	}

	inline static int32_t get_offset_of_defaultCamRotation_34() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___defaultCamRotation_34)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_defaultCamRotation_34() const { return ___defaultCamRotation_34; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_defaultCamRotation_34() { return &___defaultCamRotation_34; }
	inline void set_defaultCamRotation_34(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___defaultCamRotation_34 = value;
	}

	inline static int32_t get_offset_of_onScreenParticles_35() { return static_cast<int32_t>(offsetof(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB, ___onScreenParticles_35)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_onScreenParticles_35() const { return ___onScreenParticles_35; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_onScreenParticles_35() { return &___onScreenParticles_35; }
	inline void set_onScreenParticles_35(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___onScreenParticles_35 = value;
		Il2CppCodeGenWriteBarrier((&___onScreenParticles_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WFX_DEMO_NEW_TEB509364A67554E8243EDD6A554AEA30D860F1AB_H
#ifndef WFX_DEMO_RANDOMDIR_T82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B_H
#define WFX_DEMO_RANDOMDIR_T82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_Demo_RandomDir
struct  WFX_Demo_RandomDir_t82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 WFX_Demo_RandomDir::min
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___min_4;
	// UnityEngine.Vector3 WFX_Demo_RandomDir::max
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___max_5;

public:
	inline static int32_t get_offset_of_min_4() { return static_cast<int32_t>(offsetof(WFX_Demo_RandomDir_t82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B, ___min_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_min_4() const { return ___min_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_min_4() { return &___min_4; }
	inline void set_min_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___min_4 = value;
	}

	inline static int32_t get_offset_of_max_5() { return static_cast<int32_t>(offsetof(WFX_Demo_RandomDir_t82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B, ___max_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_max_5() const { return ___max_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_max_5() { return &___max_5; }
	inline void set_max_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___max_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WFX_DEMO_RANDOMDIR_T82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B_H
#ifndef WFX_DEMO_WALL_T0BC86E4634EB88500870A246683D7AAAACC6D1D9_H
#define WFX_DEMO_WALL_T0BC86E4634EB88500870A246683D7AAAACC6D1D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_Demo_Wall
struct  WFX_Demo_Wall_t0BC86E4634EB88500870A246683D7AAAACC6D1D9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// WFX_Demo_New WFX_Demo_Wall::demo
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB * ___demo_4;

public:
	inline static int32_t get_offset_of_demo_4() { return static_cast<int32_t>(offsetof(WFX_Demo_Wall_t0BC86E4634EB88500870A246683D7AAAACC6D1D9, ___demo_4)); }
	inline WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB * get_demo_4() const { return ___demo_4; }
	inline WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB ** get_address_of_demo_4() { return &___demo_4; }
	inline void set_demo_4(WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB * value)
	{
		___demo_4 = value;
		Il2CppCodeGenWriteBarrier((&___demo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WFX_DEMO_WALL_T0BC86E4634EB88500870A246683D7AAAACC6D1D9_H
#ifndef WFX_LIGHTFLICKER_TB8FCF286726F3C41A05FCBF8A4765EBA3C40543E_H
#define WFX_LIGHTFLICKER_TB8FCF286726F3C41A05FCBF8A4765EBA3C40543E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WFX_LightFlicker
struct  WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single WFX_LightFlicker::time
	float ___time_4;
	// System.Single WFX_LightFlicker::timer
	float ___timer_5;

public:
	inline static int32_t get_offset_of_time_4() { return static_cast<int32_t>(offsetof(WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E, ___time_4)); }
	inline float get_time_4() const { return ___time_4; }
	inline float* get_address_of_time_4() { return &___time_4; }
	inline void set_time_4(float value)
	{
		___time_4 = value;
	}

	inline static int32_t get_offset_of_timer_5() { return static_cast<int32_t>(offsetof(WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E, ___timer_5)); }
	inline float get_timer_5() const { return ___timer_5; }
	inline float* get_address_of_timer_5() { return &___timer_5; }
	inline void set_timer_5(float value)
	{
		___timer_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WFX_LIGHTFLICKER_TB8FCF286726F3C41A05FCBF8A4765EBA3C40543E_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___bodyStyle_7;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___headerStyle_8;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___footerStyle_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___bodyTexture_10;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___headerTexture_11;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___footerTexture_12;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___bodyStyle_7)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_bodyStyle_7() const { return ___bodyStyle_7; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_bodyStyle_7() { return &___bodyStyle_7; }
	inline void set_bodyStyle_7(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___bodyStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_7), value);
	}

	inline static int32_t get_offset_of_headerStyle_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___headerStyle_8)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_headerStyle_8() const { return ___headerStyle_8; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_headerStyle_8() { return &___headerStyle_8; }
	inline void set_headerStyle_8(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___headerStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_8), value);
	}

	inline static int32_t get_offset_of_footerStyle_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___footerStyle_9)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_footerStyle_9() const { return ___footerStyle_9; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_footerStyle_9() { return &___footerStyle_9; }
	inline void set_footerStyle_9(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___footerStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_9), value);
	}

	inline static int32_t get_offset_of_bodyTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___bodyTexture_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_bodyTexture_10() const { return ___bodyTexture_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_bodyTexture_10() { return &___bodyTexture_10; }
	inline void set_bodyTexture_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___bodyTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_10), value);
	}

	inline static int32_t get_offset_of_headerTexture_11() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___headerTexture_11)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_headerTexture_11() const { return ___headerTexture_11; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_headerTexture_11() { return &___headerTexture_11; }
	inline void set_headerTexture_11(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___headerTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_11), value);
	}

	inline static int32_t get_offset_of_footerTexture_12() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___footerTexture_12)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_footerTexture_12() const { return ___footerTexture_12; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_footerTexture_12() { return &___footerTexture_12; }
	inline void set_footerTexture_12(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___footerTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H
#ifndef USERDEFINEDTARGETBUILDINGBEHAVIOUR_T0712EA02EA85D448B84DBE79577CD2F2874F2CA7_H
#define USERDEFINEDTARGETBUILDINGBEHAVIOUR_T0712EA02EA85D448B84DBE79577CD2F2874F2CA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UserDefinedTargetBuildingBehaviour
struct  UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// Vuforia.ObjectTracker Vuforia.UserDefinedTargetBuildingBehaviour::mObjectTracker
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E * ___mObjectTracker_4;
	// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.UserDefinedTargetBuildingBehaviour::mLastFrameQuality
	int32_t ___mLastFrameQuality_5;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyScanning
	bool ___mCurrentlyScanning_6;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasScanningBeforeDisable
	bool ___mWasScanningBeforeDisable_7;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mCurrentlyBuilding
	bool ___mCurrentlyBuilding_8;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mWasBuildingBeforeDisable
	bool ___mWasBuildingBeforeDisable_9;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_10;
	// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler> Vuforia.UserDefinedTargetBuildingBehaviour::mHandlers
	List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B * ___mHandlers_11;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopTrackerWhileScanning
	bool ___StopTrackerWhileScanning_12;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StartScanningAutomatically
	bool ___StartScanningAutomatically_13;
	// System.Boolean Vuforia.UserDefinedTargetBuildingBehaviour::StopScanningWhenFinshedBuilding
	bool ___StopScanningWhenFinshedBuilding_14;

public:
	inline static int32_t get_offset_of_mObjectTracker_4() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mObjectTracker_4)); }
	inline ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E * get_mObjectTracker_4() const { return ___mObjectTracker_4; }
	inline ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E ** get_address_of_mObjectTracker_4() { return &___mObjectTracker_4; }
	inline void set_mObjectTracker_4(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E * value)
	{
		___mObjectTracker_4 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_4), value);
	}

	inline static int32_t get_offset_of_mLastFrameQuality_5() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mLastFrameQuality_5)); }
	inline int32_t get_mLastFrameQuality_5() const { return ___mLastFrameQuality_5; }
	inline int32_t* get_address_of_mLastFrameQuality_5() { return &___mLastFrameQuality_5; }
	inline void set_mLastFrameQuality_5(int32_t value)
	{
		___mLastFrameQuality_5 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyScanning_6() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mCurrentlyScanning_6)); }
	inline bool get_mCurrentlyScanning_6() const { return ___mCurrentlyScanning_6; }
	inline bool* get_address_of_mCurrentlyScanning_6() { return &___mCurrentlyScanning_6; }
	inline void set_mCurrentlyScanning_6(bool value)
	{
		___mCurrentlyScanning_6 = value;
	}

	inline static int32_t get_offset_of_mWasScanningBeforeDisable_7() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mWasScanningBeforeDisable_7)); }
	inline bool get_mWasScanningBeforeDisable_7() const { return ___mWasScanningBeforeDisable_7; }
	inline bool* get_address_of_mWasScanningBeforeDisable_7() { return &___mWasScanningBeforeDisable_7; }
	inline void set_mWasScanningBeforeDisable_7(bool value)
	{
		___mWasScanningBeforeDisable_7 = value;
	}

	inline static int32_t get_offset_of_mCurrentlyBuilding_8() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mCurrentlyBuilding_8)); }
	inline bool get_mCurrentlyBuilding_8() const { return ___mCurrentlyBuilding_8; }
	inline bool* get_address_of_mCurrentlyBuilding_8() { return &___mCurrentlyBuilding_8; }
	inline void set_mCurrentlyBuilding_8(bool value)
	{
		___mCurrentlyBuilding_8 = value;
	}

	inline static int32_t get_offset_of_mWasBuildingBeforeDisable_9() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mWasBuildingBeforeDisable_9)); }
	inline bool get_mWasBuildingBeforeDisable_9() const { return ___mWasBuildingBeforeDisable_9; }
	inline bool* get_address_of_mWasBuildingBeforeDisable_9() { return &___mWasBuildingBeforeDisable_9; }
	inline void set_mWasBuildingBeforeDisable_9(bool value)
	{
		___mWasBuildingBeforeDisable_9 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_10() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mOnInitializedCalled_10)); }
	inline bool get_mOnInitializedCalled_10() const { return ___mOnInitializedCalled_10; }
	inline bool* get_address_of_mOnInitializedCalled_10() { return &___mOnInitializedCalled_10; }
	inline void set_mOnInitializedCalled_10(bool value)
	{
		___mOnInitializedCalled_10 = value;
	}

	inline static int32_t get_offset_of_mHandlers_11() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___mHandlers_11)); }
	inline List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B * get_mHandlers_11() const { return ___mHandlers_11; }
	inline List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B ** get_address_of_mHandlers_11() { return &___mHandlers_11; }
	inline void set_mHandlers_11(List_1_t0748E6C49D36B6B078F5DBEDEAEDDE881580AD0B * value)
	{
		___mHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_11), value);
	}

	inline static int32_t get_offset_of_StopTrackerWhileScanning_12() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___StopTrackerWhileScanning_12)); }
	inline bool get_StopTrackerWhileScanning_12() const { return ___StopTrackerWhileScanning_12; }
	inline bool* get_address_of_StopTrackerWhileScanning_12() { return &___StopTrackerWhileScanning_12; }
	inline void set_StopTrackerWhileScanning_12(bool value)
	{
		___StopTrackerWhileScanning_12 = value;
	}

	inline static int32_t get_offset_of_StartScanningAutomatically_13() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___StartScanningAutomatically_13)); }
	inline bool get_StartScanningAutomatically_13() const { return ___StartScanningAutomatically_13; }
	inline bool* get_address_of_StartScanningAutomatically_13() { return &___StartScanningAutomatically_13; }
	inline void set_StartScanningAutomatically_13(bool value)
	{
		___StartScanningAutomatically_13 = value;
	}

	inline static int32_t get_offset_of_StopScanningWhenFinshedBuilding_14() { return static_cast<int32_t>(offsetof(UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7, ___StopScanningWhenFinshedBuilding_14)); }
	inline bool get_StopScanningWhenFinshedBuilding_14() const { return ___StopScanningWhenFinshedBuilding_14; }
	inline bool* get_address_of_StopScanningWhenFinshedBuilding_14() { return &___StopScanningWhenFinshedBuilding_14; }
	inline void set_StopScanningWhenFinshedBuilding_14(bool value)
	{
		___StopScanningWhenFinshedBuilding_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDEFINEDTARGETBUILDINGBEHAVIOUR_T0712EA02EA85D448B84DBE79577CD2F2874F2CA7_H
#ifndef VIDEOBACKGROUNDBEHAVIOUR_T5FE09B8C02AFCA301B6D915668BE3C050CE37C00_H
#define VIDEOBACKGROUNDBEHAVIOUR_T5FE09B8C02AFCA301B6D915668BE3C050CE37C00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VideoBackgroundBehaviour
struct  VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mClearBuffers
	int32_t ___mClearBuffers_4;
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mSkipStateUpdates
	int32_t ___mSkipStateUpdates_5;
	// Vuforia.VuforiaARController Vuforia.VideoBackgroundBehaviour::mVuforiaARController
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * ___mVuforiaARController_6;
	// UnityEngine.Camera Vuforia.VideoBackgroundBehaviour::mCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mCamera_7;
	// Vuforia.BackgroundPlaneBehaviour Vuforia.VideoBackgroundBehaviour::mBackgroundBehaviour
	BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * ___mBackgroundBehaviour_8;
	// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer> Vuforia.VideoBackgroundBehaviour::mDisabledMeshRenderers
	HashSet_1_t173E251C9CF6D9E02C6BF71D05C0DD07220A28A9 * ___mDisabledMeshRenderers_11;

public:
	inline static int32_t get_offset_of_mClearBuffers_4() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mClearBuffers_4)); }
	inline int32_t get_mClearBuffers_4() const { return ___mClearBuffers_4; }
	inline int32_t* get_address_of_mClearBuffers_4() { return &___mClearBuffers_4; }
	inline void set_mClearBuffers_4(int32_t value)
	{
		___mClearBuffers_4 = value;
	}

	inline static int32_t get_offset_of_mSkipStateUpdates_5() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mSkipStateUpdates_5)); }
	inline int32_t get_mSkipStateUpdates_5() const { return ___mSkipStateUpdates_5; }
	inline int32_t* get_address_of_mSkipStateUpdates_5() { return &___mSkipStateUpdates_5; }
	inline void set_mSkipStateUpdates_5(int32_t value)
	{
		___mSkipStateUpdates_5 = value;
	}

	inline static int32_t get_offset_of_mVuforiaARController_6() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mVuforiaARController_6)); }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * get_mVuforiaARController_6() const { return ___mVuforiaARController_6; }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 ** get_address_of_mVuforiaARController_6() { return &___mVuforiaARController_6; }
	inline void set_mVuforiaARController_6(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * value)
	{
		___mVuforiaARController_6 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaARController_6), value);
	}

	inline static int32_t get_offset_of_mCamera_7() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mCamera_7)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mCamera_7() const { return ___mCamera_7; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mCamera_7() { return &___mCamera_7; }
	inline void set_mCamera_7(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_7), value);
	}

	inline static int32_t get_offset_of_mBackgroundBehaviour_8() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mBackgroundBehaviour_8)); }
	inline BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * get_mBackgroundBehaviour_8() const { return ___mBackgroundBehaviour_8; }
	inline BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 ** get_address_of_mBackgroundBehaviour_8() { return &___mBackgroundBehaviour_8; }
	inline void set_mBackgroundBehaviour_8(BackgroundPlaneBehaviour_t2615248F9F83AF94A06C6585EBB6C1D3BCF64338 * value)
	{
		___mBackgroundBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundBehaviour_8), value);
	}

	inline static int32_t get_offset_of_mDisabledMeshRenderers_11() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00, ___mDisabledMeshRenderers_11)); }
	inline HashSet_1_t173E251C9CF6D9E02C6BF71D05C0DD07220A28A9 * get_mDisabledMeshRenderers_11() const { return ___mDisabledMeshRenderers_11; }
	inline HashSet_1_t173E251C9CF6D9E02C6BF71D05C0DD07220A28A9 ** get_address_of_mDisabledMeshRenderers_11() { return &___mDisabledMeshRenderers_11; }
	inline void set_mDisabledMeshRenderers_11(HashSet_1_t173E251C9CF6D9E02C6BF71D05C0DD07220A28A9 * value)
	{
		___mDisabledMeshRenderers_11 = value;
		Il2CppCodeGenWriteBarrier((&___mDisabledMeshRenderers_11), value);
	}
};

struct VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00_StaticFields
{
public:
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mFrameCounter
	int32_t ___mFrameCounter_9;
	// System.Int32 Vuforia.VideoBackgroundBehaviour::mRenderCounter
	int32_t ___mRenderCounter_10;

public:
	inline static int32_t get_offset_of_mFrameCounter_9() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00_StaticFields, ___mFrameCounter_9)); }
	inline int32_t get_mFrameCounter_9() const { return ___mFrameCounter_9; }
	inline int32_t* get_address_of_mFrameCounter_9() { return &___mFrameCounter_9; }
	inline void set_mFrameCounter_9(int32_t value)
	{
		___mFrameCounter_9 = value;
	}

	inline static int32_t get_offset_of_mRenderCounter_10() { return static_cast<int32_t>(offsetof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00_StaticFields, ___mRenderCounter_10)); }
	inline int32_t get_mRenderCounter_10() const { return ___mRenderCounter_10; }
	inline int32_t* get_address_of_mRenderCounter_10() { return &___mRenderCounter_10; }
	inline void set_mRenderCounter_10(int32_t value)
	{
		___mRenderCounter_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDBEHAVIOUR_T5FE09B8C02AFCA301B6D915668BE3C050CE37C00_H
#ifndef VIRTUALBUTTONBEHAVIOUR_TD6CB39E767B87A0527253A412F4DB6F813BD6FEE_H
#define VIRTUALBUTTONBEHAVIOUR_TD6CB39E767B87A0527253A412F4DB6F813BD6FEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VirtualButtonBehaviour
struct  VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.String Vuforia.VirtualButtonBehaviour::mName
	String_t* ___mName_5;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonBehaviour::mSensitivity
	int32_t ___mSensitivity_6;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mHasUpdatedPose
	bool ___mHasUpdatedPose_7;
	// UnityEngine.Matrix4x4 Vuforia.VirtualButtonBehaviour::mPrevTransform
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___mPrevTransform_8;
	// UnityEngine.GameObject Vuforia.VirtualButtonBehaviour::mPrevParent
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mPrevParent_9;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mSensitivityDirty
	bool ___mSensitivityDirty_10;
	// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonBehaviour::mPreviousSensitivity
	int32_t ___mPreviousSensitivity_11;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPreviouslyEnabled
	bool ___mPreviouslyEnabled_12;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mPressed
	bool ___mPressed_13;
	// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler> Vuforia.VirtualButtonBehaviour::mHandlers
	List_1_tE1500C473509EDCE2ED838547DF41114B3785932 * ___mHandlers_14;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mLeftTop
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mLeftTop_15;
	// UnityEngine.Vector2 Vuforia.VirtualButtonBehaviour::mRightBottom
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mRightBottom_16;
	// System.Boolean Vuforia.VirtualButtonBehaviour::mUnregisterOnDestroy
	bool ___mUnregisterOnDestroy_17;
	// Vuforia.VirtualButton Vuforia.VirtualButtonBehaviour::mVirtualButton
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654 * ___mVirtualButton_18;

public:
	inline static int32_t get_offset_of_mName_5() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mName_5)); }
	inline String_t* get_mName_5() const { return ___mName_5; }
	inline String_t** get_address_of_mName_5() { return &___mName_5; }
	inline void set_mName_5(String_t* value)
	{
		___mName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mName_5), value);
	}

	inline static int32_t get_offset_of_mSensitivity_6() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mSensitivity_6)); }
	inline int32_t get_mSensitivity_6() const { return ___mSensitivity_6; }
	inline int32_t* get_address_of_mSensitivity_6() { return &___mSensitivity_6; }
	inline void set_mSensitivity_6(int32_t value)
	{
		___mSensitivity_6 = value;
	}

	inline static int32_t get_offset_of_mHasUpdatedPose_7() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mHasUpdatedPose_7)); }
	inline bool get_mHasUpdatedPose_7() const { return ___mHasUpdatedPose_7; }
	inline bool* get_address_of_mHasUpdatedPose_7() { return &___mHasUpdatedPose_7; }
	inline void set_mHasUpdatedPose_7(bool value)
	{
		___mHasUpdatedPose_7 = value;
	}

	inline static int32_t get_offset_of_mPrevTransform_8() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPrevTransform_8)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_mPrevTransform_8() const { return ___mPrevTransform_8; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_mPrevTransform_8() { return &___mPrevTransform_8; }
	inline void set_mPrevTransform_8(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___mPrevTransform_8 = value;
	}

	inline static int32_t get_offset_of_mPrevParent_9() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPrevParent_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mPrevParent_9() const { return ___mPrevParent_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mPrevParent_9() { return &___mPrevParent_9; }
	inline void set_mPrevParent_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mPrevParent_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPrevParent_9), value);
	}

	inline static int32_t get_offset_of_mSensitivityDirty_10() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mSensitivityDirty_10)); }
	inline bool get_mSensitivityDirty_10() const { return ___mSensitivityDirty_10; }
	inline bool* get_address_of_mSensitivityDirty_10() { return &___mSensitivityDirty_10; }
	inline void set_mSensitivityDirty_10(bool value)
	{
		___mSensitivityDirty_10 = value;
	}

	inline static int32_t get_offset_of_mPreviousSensitivity_11() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPreviousSensitivity_11)); }
	inline int32_t get_mPreviousSensitivity_11() const { return ___mPreviousSensitivity_11; }
	inline int32_t* get_address_of_mPreviousSensitivity_11() { return &___mPreviousSensitivity_11; }
	inline void set_mPreviousSensitivity_11(int32_t value)
	{
		___mPreviousSensitivity_11 = value;
	}

	inline static int32_t get_offset_of_mPreviouslyEnabled_12() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPreviouslyEnabled_12)); }
	inline bool get_mPreviouslyEnabled_12() const { return ___mPreviouslyEnabled_12; }
	inline bool* get_address_of_mPreviouslyEnabled_12() { return &___mPreviouslyEnabled_12; }
	inline void set_mPreviouslyEnabled_12(bool value)
	{
		___mPreviouslyEnabled_12 = value;
	}

	inline static int32_t get_offset_of_mPressed_13() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mPressed_13)); }
	inline bool get_mPressed_13() const { return ___mPressed_13; }
	inline bool* get_address_of_mPressed_13() { return &___mPressed_13; }
	inline void set_mPressed_13(bool value)
	{
		___mPressed_13 = value;
	}

	inline static int32_t get_offset_of_mHandlers_14() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mHandlers_14)); }
	inline List_1_tE1500C473509EDCE2ED838547DF41114B3785932 * get_mHandlers_14() const { return ___mHandlers_14; }
	inline List_1_tE1500C473509EDCE2ED838547DF41114B3785932 ** get_address_of_mHandlers_14() { return &___mHandlers_14; }
	inline void set_mHandlers_14(List_1_tE1500C473509EDCE2ED838547DF41114B3785932 * value)
	{
		___mHandlers_14 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_14), value);
	}

	inline static int32_t get_offset_of_mLeftTop_15() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mLeftTop_15)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mLeftTop_15() const { return ___mLeftTop_15; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mLeftTop_15() { return &___mLeftTop_15; }
	inline void set_mLeftTop_15(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mLeftTop_15 = value;
	}

	inline static int32_t get_offset_of_mRightBottom_16() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mRightBottom_16)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mRightBottom_16() const { return ___mRightBottom_16; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mRightBottom_16() { return &___mRightBottom_16; }
	inline void set_mRightBottom_16(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mRightBottom_16 = value;
	}

	inline static int32_t get_offset_of_mUnregisterOnDestroy_17() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mUnregisterOnDestroy_17)); }
	inline bool get_mUnregisterOnDestroy_17() const { return ___mUnregisterOnDestroy_17; }
	inline bool* get_address_of_mUnregisterOnDestroy_17() { return &___mUnregisterOnDestroy_17; }
	inline void set_mUnregisterOnDestroy_17(bool value)
	{
		___mUnregisterOnDestroy_17 = value;
	}

	inline static int32_t get_offset_of_mVirtualButton_18() { return static_cast<int32_t>(offsetof(VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE, ___mVirtualButton_18)); }
	inline VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654 * get_mVirtualButton_18() const { return ___mVirtualButton_18; }
	inline VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654 ** get_address_of_mVirtualButton_18() { return &___mVirtualButton_18; }
	inline void set_mVirtualButton_18(VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654 * value)
	{
		___mVirtualButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButton_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIRTUALBUTTONBEHAVIOUR_TD6CB39E767B87A0527253A412F4DB6F813BD6FEE_H
#ifndef WIREFRAMEBEHAVIOUR_T4D7E355A49179D0FA85D66D925AD5832274D939F_H
#define WIREFRAMEBEHAVIOUR_T4D7E355A49179D0FA85D66D925AD5832274D939F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeBehaviour
struct  WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// UnityEngine.Material Vuforia.WireframeBehaviour::lineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___lineMaterial_4;
	// System.Boolean Vuforia.WireframeBehaviour::ShowLines
	bool ___ShowLines_5;
	// UnityEngine.Color Vuforia.WireframeBehaviour::LineColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___LineColor_6;
	// UnityEngine.Material Vuforia.WireframeBehaviour::mLineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mLineMaterial_7;

public:
	inline static int32_t get_offset_of_lineMaterial_4() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F, ___lineMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_lineMaterial_4() const { return ___lineMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_lineMaterial_4() { return &___lineMaterial_4; }
	inline void set_lineMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___lineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineMaterial_4), value);
	}

	inline static int32_t get_offset_of_ShowLines_5() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F, ___ShowLines_5)); }
	inline bool get_ShowLines_5() const { return ___ShowLines_5; }
	inline bool* get_address_of_ShowLines_5() { return &___ShowLines_5; }
	inline void set_ShowLines_5(bool value)
	{
		___ShowLines_5 = value;
	}

	inline static int32_t get_offset_of_LineColor_6() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F, ___LineColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_LineColor_6() const { return ___LineColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_LineColor_6() { return &___LineColor_6; }
	inline void set_LineColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___LineColor_6 = value;
	}

	inline static int32_t get_offset_of_mLineMaterial_7() { return static_cast<int32_t>(offsetof(WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F, ___mLineMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_mLineMaterial_7() const { return ___mLineMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_mLineMaterial_7() { return &___mLineMaterial_7; }
	inline void set_mLineMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___mLineMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMEBEHAVIOUR_T4D7E355A49179D0FA85D66D925AD5832274D939F_H
#ifndef WIREFRAMETRACKABLEEVENTHANDLER_TB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13_H
#define WIREFRAMETRACKABLEEVENTHANDLER_TB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WireframeTrackableEventHandler
struct  WireframeTrackableEventHandler_tB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// Vuforia.TrackableBehaviour Vuforia.WireframeTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(WireframeTrackableEventHandler_tB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIREFRAMETRACKABLEEVENTHANDLER_TB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530), -1, sizeof(TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2900[5] = 
{
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530_StaticFields::get_offset_of_mInstance_0(),
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530::get_offset_of_mStateManager_1(),
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530::get_offset_of_mTrackers_2(),
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530::get_offset_of_mTrackerCreators_3(),
	TrackerManager_t4E97F9E410AEF74F864668111D4E1AE7B8EA7530::get_offset_of_mTrackerNativeDeinitializers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370), -1, sizeof(U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2901[5] = 
{
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9__8_2_3(),
	U3CU3Ec_tB15984925A821B29BE48F5B2F3E523834E5E9370_StaticFields::get_offset_of_U3CU3E9__8_3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2903[11] = 
{
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mObjectTracker_4(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mLastFrameQuality_5(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mCurrentlyScanning_6(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mWasScanningBeforeDisable_7(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mCurrentlyBuilding_8(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mWasBuildingBeforeDisable_9(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mOnInitializedCalled_10(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_mHandlers_11(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_StopTrackerWhileScanning_12(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_StartScanningAutomatically_13(),
	UserDefinedTargetBuildingBehaviour_t0712EA02EA85D448B84DBE79577CD2F2874F2CA7::get_offset_of_StopScanningWhenFinshedBuilding_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00), -1, sizeof(VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2904[8] = 
{
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mClearBuffers_4(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mSkipStateUpdates_5(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mVuforiaARController_6(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mCamera_7(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mBackgroundBehaviour_8(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00_StaticFields::get_offset_of_mFrameCounter_9(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00_StaticFields::get_offset_of_mRenderCounter_10(),
	VideoBackgroundBehaviour_t5FE09B8C02AFCA301B6D915668BE3C050CE37C00::get_offset_of_mDisabledMeshRenderers_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50), -1, sizeof(VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2905[8] = 
{
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mTexture_4(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_tDCE9A898A17B3C5ED63C8BDE29A4455C4074BD50_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[7] = 
{
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mName_0(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mID_1(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mArea_2(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mIsEnabled_3(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mParentImageTarget_4(),
	VirtualButton_t95A02A5A354F3D3274398C6CEE3CC0E96E38D654::get_offset_of_mParentDataSet_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (Sensitivity_t7654EFB20C36491C60EAA2D010FAA6A0D9D14B01)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2907[4] = 
{
	Sensitivity_t7654EFB20C36491C60EAA2D010FAA6A0D9D14B01::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[15] = 
{
	0,
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mName_5(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mSensitivity_6(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mHasUpdatedPose_7(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPrevTransform_8(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPrevParent_9(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mSensitivityDirty_10(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPreviousSensitivity_11(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPreviouslyEnabled_12(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mPressed_13(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mHandlers_14(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mLeftTop_15(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mRightBottom_16(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mUnregisterOnDestroy_17(),
	VirtualButtonBehaviour_tD6CB39E767B87A0527253A412F4DB6F813BD6FEE::get_offset_of_mVirtualButton_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A), -1, sizeof(WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2909[7] = 
{
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A::get_offset_of_mWebCamTexAdaptorProvider_5(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields::get_offset_of_mInstance_6(),
	WebCamARController_t927205B62F0EDCE72569FA4CA0D034E6BC804D1A_StaticFields::get_offset_of_mPadlock_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB), -1, sizeof(U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2910[2] = 
{
	U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7F5A80A4ED3D4E5058582D2682AC998995E82FDB_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (EyewearCalibrationProfileManager_tDC16C656B1E63F9F7FA7A165262CF001CFBF672A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (EyewearUserCalibrator_tFA27D2BE8A64087E551C93ADE2489D69A0A79A49), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[4] = 
{
	WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F::get_offset_of_lineMaterial_4(),
	WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F::get_offset_of_ShowLines_5(),
	WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F::get_offset_of_LineColor_6(),
	WireframeBehaviour_t4D7E355A49179D0FA85D66D925AD5832274D939F::get_offset_of_mLineMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (WireframeTrackableEventHandler_tB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[1] = 
{
	WireframeTrackableEventHandler_tB39EFA88CDD4D927925C8E1DCE4AEDBC32F8FF13::get_offset_of_mTrackableBehaviour_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47), -1, sizeof(U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2915[1] = 
{
	U3CPrivateImplementationDetailsU3E_t6817CFECEB78EAB6A13B460969C0CEAD9F899C47_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (__StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_tD7B241978857C817DC5F90FD7E79EBA548903A7E ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (U3CModuleU3E_tCE4B768174CDE0294B05DD8ED59A7763FF34E99B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (U3CModuleU3E_tBB5D6FDE6CBE0067E5DC774822D5054F01745F8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[9] = 
{
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_mErrorText_4(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_mErrorOccurred_5(),
	0,
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_bodyStyle_7(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_headerStyle_8(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_footerStyle_9(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_bodyTexture_10(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_headerTexture_11(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_footerTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2920[3] = 
{
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_mTrackableBehaviour_4(),
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_m_PreviousStatus_5(),
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_m_NewStatus_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[1] = 
{
	DecalDestroyer_tB4266C3B20B58251EC4488060F2DDBBBFD647F0E::get_offset_of_lifeTime_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (U3CStartU3Ed__1_t18E1B08206054478B6F37BB254E8005B78622625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2923[3] = 
{
	U3CStartU3Ed__1_t18E1B08206054478B6F37BB254E8005B78622625::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__1_t18E1B08206054478B6F37BB254E8005B78622625::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__1_t18E1B08206054478B6F37BB254E8005B78622625::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD::get_offset_of_fireParticleSystem_4(),
	ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD::get_offset_of_smokeParticleSystem_5(),
	ExtinguishableFire_t6181DD4DBE99311FEE56DD85204B2C80D8EE0BFD::get_offset_of_m_isExtinguished_6(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2925[4] = 
{
	U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191::get_offset_of_U3CU3E1__state_0(),
	U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191::get_offset_of_U3CU3E2__current_1(),
	U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191::get_offset_of_U3CU3E4__this_2(),
	U3CExtinguishingU3Ed__6_t1CD8B6B221AE581CBC9C5E2A6656E06E8F8E9191::get_offset_of_U3CelapsedTimeU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[4] = 
{
	U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F::get_offset_of_U3CU3E1__state_0(),
	U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F::get_offset_of_U3CU3E2__current_1(),
	U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F::get_offset_of_U3CU3E4__this_2(),
	U3CStartingFireU3Ed__7_tBFAD7A923C81995633CAE044A53DC33229EF873F::get_offset_of_U3CelapsedTimeU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[6] = 
{
	GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880::get_offset_of_borderLeft_4(),
	GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880::get_offset_of_borderRight_5(),
	GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880::get_offset_of_borderTop_6(),
	GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880::get_offset_of_borderBottom_7(),
	GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880::get_offset_of_parentCamera_8(),
	GunAim_tEA32268B21D5E39AD6A0E0C3BF23115E65704880::get_offset_of_isOutOfBounds_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2928[15] = 
{
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_fireRate_4(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_weaponRange_5(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_gunEnd_6(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_muzzleFlash_7(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_cartridgeEjection_8(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_metalHitEffect_9(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_sandHitEffect_10(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_stoneHitEffect_11(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_waterLeakEffect_12(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_waterLeakExtinguishEffect_13(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_fleshHitEffects_14(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_woodHitEffect_15(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_nextFire_16(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_anim_17(),
	GunShoot_tEEFCDD0F1E738400870487F8710752C9EC799755::get_offset_of_gunAim_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (ParticleCollision_tCCB7E309526592B462EDEB34AE317959D704C730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2929[2] = 
{
	ParticleCollision_tCCB7E309526592B462EDEB34AE317959D704C730::get_offset_of_m_CollisionEvents_4(),
	ParticleCollision_tCCB7E309526592B462EDEB34AE317959D704C730::get_offset_of_m_ParticleSystem_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2930[6] = 
{
	ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190::get_offset_of_title_0(),
	ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190::get_offset_of_description_1(),
	ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190::get_offset_of_isWeaponEffect_2(),
	ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190::get_offset_of_particleSystemGO_3(),
	ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190::get_offset_of_particlePosition_4(),
	ParticleExamples_t26D5EB932FF7F2D069A066370B9340604A938190::get_offset_of_particleRotation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[8] = 
{
	ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7::get_offset_of_particleSystems_4(),
	ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7::get_offset_of_gunGameObject_5(),
	ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7::get_offset_of_currentIndex_6(),
	ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7::get_offset_of_currentGO_7(),
	ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7::get_offset_of_spawnLocation_8(),
	ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7::get_offset_of_title_9(),
	ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7::get_offset_of_description_10(),
	ParticleMenu_t7B899AF8E3B15C4C19D7E482ECCB80FED3664CA7::get_offset_of_navigationDetails_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2932[4] = 
{
	Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46::get_offset_of_icon_4(),
	Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46::get_offset_of_title_5(),
	Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46::get_offset_of_sections_6(),
	Readme_t3B6022BA9CBD2C56BD12C781DB20C5B34B6C4E46::get_offset_of_loadedLayout_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2933[4] = 
{
	Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985::get_offset_of_heading_0(),
	Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985::get_offset_of_text_1(),
	Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985::get_offset_of_linkText_2(),
	Section_tDCE8FED365051651AC2BFB27F2230DDC5603C985::get_offset_of_url_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (CFX_AutoStopLoopedEffect_tDF281CCB89367AEE38D90CDF62617F85A9316A7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2934[2] = 
{
	CFX_AutoStopLoopedEffect_tDF281CCB89367AEE38D90CDF62617F85A9316A7F::get_offset_of_effectDuration_4(),
	CFX_AutoStopLoopedEffect_tDF281CCB89367AEE38D90CDF62617F85A9316A7F::get_offset_of_d_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (CFX_Demo_RandomDir_t60CF3850799F52A2210DA9A0434DFAB5D88D07CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2935[2] = 
{
	CFX_Demo_RandomDir_t60CF3850799F52A2210DA9A0434DFAB5D88D07CC::get_offset_of_min_4(),
	CFX_Demo_RandomDir_t60CF3850799F52A2210DA9A0434DFAB5D88D07CC::get_offset_of_max_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354), -1, sizeof(CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2936[3] = 
{
	CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354_StaticFields::get_offset_of_rotating_4(),
	CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354::get_offset_of_speed_5(),
	CFX_Demo_RotateCamera_tB84DB0DAACDDD78C6BBDDB4365EC775B90961354::get_offset_of_rotationCenter_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2937[5] = 
{
	CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F::get_offset_of_speed_4(),
	CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F::get_offset_of_rotation_5(),
	CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F::get_offset_of_axis_6(),
	CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F::get_offset_of_gravity_7(),
	CFX_Demo_Translate_t62611C2FF6F408278CE320E134FC59F15E5C3D3F::get_offset_of_dir_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { sizeof (WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[26] = 
{
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_cameraSpeed_4(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_orderedSpawns_5(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_step_6(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_range_7(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_order_8(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_walls_9(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_bulletholes_10(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_ParticleExamples_11(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_exampleIndex_12(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_randomSpawnsDelay_13(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_randomSpawns_14(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_slowMo_15(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_rotateCam_16(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_wood_17(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_concrete_18(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_metal_19(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_checker_20(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_woodWall_21(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_concreteWall_22(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_metalWall_23(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_checkerWall_24(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_groundTextureStr_25(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_groundTextures_26(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_m4_27(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_m4fps_28(),
	WFX_Demo_t45A38B9635A0B3C6FAB09DE3AC4D9432C9F6858E::get_offset_of_rotate_m4_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (U3CRandomSpawnsCoroutineU3Ed__30_t529E8BF318623FFB6193BA10C31B081E1442B941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[3] = 
{
	U3CRandomSpawnsCoroutineU3Ed__30_t529E8BF318623FFB6193BA10C31B081E1442B941::get_offset_of_U3CU3E1__state_0(),
	U3CRandomSpawnsCoroutineU3Ed__30_t529E8BF318623FFB6193BA10C31B081E1442B941::get_offset_of_U3CU3E2__current_1(),
	U3CRandomSpawnsCoroutineU3Ed__30_t529E8BF318623FFB6193BA10C31B081E1442B941::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (WFX_Demo_DeleteAfterDelay_tA71701362CD4B07A41161C3BFCE2B4882E2DC2EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2940[1] = 
{
	WFX_Demo_DeleteAfterDelay_tA71701362CD4B07A41161C3BFCE2B4882E2DC2EB::get_offset_of_delay_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2941[32] = 
{
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_groundRenderer_4(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_groundCollider_5(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_slowMoBtn_6(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_slowMoLabel_7(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_camRotBtn_8(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_camRotLabel_9(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_groundBtn_10(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_groundLabel_11(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_EffectLabel_12(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_EffectIndexLabel_13(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_AdditionalEffects_14(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_ground_15(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_walls_16(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_bulletholes_17(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_m4_18(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_m4fps_19(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_wood_20(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_concrete_21(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_metal_22(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_checker_23(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_woodWall_24(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_concreteWall_25(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_metalWall_26(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_checkerWall_27(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_groundTextureStr_28(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_groundTextures_29(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_ParticleExamples_30(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_exampleIndex_31(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_slowMo_32(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_defaultCamPosition_33(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_defaultCamRotation_34(),
	WFX_Demo_New_tEB509364A67554E8243EDD6A554AEA30D860F1AB::get_offset_of_onScreenParticles_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (U3CCheckForDeletedParticlesU3Ed__41_t159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2942[3] = 
{
	U3CCheckForDeletedParticlesU3Ed__41_t159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9::get_offset_of_U3CU3E1__state_0(),
	U3CCheckForDeletedParticlesU3Ed__41_t159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9::get_offset_of_U3CU3E2__current_1(),
	U3CCheckForDeletedParticlesU3Ed__41_t159F26A99D8B0ED8C7490EC3C6649B96BBC64EB9::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (WFX_Demo_RandomDir_t82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[2] = 
{
	WFX_Demo_RandomDir_t82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B::get_offset_of_min_4(),
	WFX_Demo_RandomDir_t82FE2A7361D30B380CDDF7B49AFCB853B3D69E8B::get_offset_of_max_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (WFX_Demo_Wall_t0BC86E4634EB88500870A246683D7AAAACC6D1D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[1] = 
{
	WFX_Demo_Wall_t0BC86E4634EB88500870A246683D7AAAACC6D1D9::get_offset_of_demo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { sizeof (CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2945[1] = 
{
	CFX_AutoDestructShuriken_tF656F157683CC094608046496754C72A910D10F5::get_offset_of_OnlyDeactivate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (U3CCheckIfAliveU3Ed__2_tF5EA91DDEAE9EB6B7FAF0D313578E96C08450542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[3] = 
{
	U3CCheckIfAliveU3Ed__2_tF5EA91DDEAE9EB6B7FAF0D313578E96C08450542::get_offset_of_U3CU3E1__state_0(),
	U3CCheckIfAliveU3Ed__2_tF5EA91DDEAE9EB6B7FAF0D313578E96C08450542::get_offset_of_U3CU3E2__current_1(),
	U3CCheckIfAliveU3Ed__2_tF5EA91DDEAE9EB6B7FAF0D313578E96C08450542::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2947[7] = 
{
	CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12::get_offset_of_duration_4(),
	CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12::get_offset_of_delay_5(),
	CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12::get_offset_of_finalIntensity_6(),
	CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12::get_offset_of_baseIntensity_7(),
	CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12::get_offset_of_autodestruct_8(),
	CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12::get_offset_of_p_lifetime_9(),
	CFX_LightIntensityFade_t0AC70576F12AE545F5A72AF67C99CDE6711B3C12::get_offset_of_p_delay_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5), -1, sizeof(WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2948[10] = 
{
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5_StaticFields::get_offset_of_quadUVs_4(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_lifetime_5(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_fadeoutpercent_6(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_frames_7(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_randomRotation_8(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_deactivate_9(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_life_10(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_fadeout_11(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_color_12(),
	WFX_BulletHoleDecal_t75BB8417732D09D9092DE9D8A3A30B3EABE376F5::get_offset_of_orgAlpha_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (U3CholeUpdateU3Ed__12_tE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[3] = 
{
	U3CholeUpdateU3Ed__12_tE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F::get_offset_of_U3CU3E1__state_0(),
	U3CholeUpdateU3Ed__12_tE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F::get_offset_of_U3CU3E2__current_1(),
	U3CholeUpdateU3Ed__12_tE213FA0CDD847F5AA1B4801B0BD3B8109EF6344F::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[2] = 
{
	WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E::get_offset_of_time_4(),
	WFX_LightFlicker_tB8FCF286726F3C41A05FCBF8A4765EBA3C40543E::get_offset_of_timer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (U3CFlickerU3Ed__3_tDBCB5374078529EF8B3F7BC65291F8A27D0EADE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2951[3] = 
{
	U3CFlickerU3Ed__3_tDBCB5374078529EF8B3F7BC65291F8A27D0EADE7::get_offset_of_U3CU3E1__state_0(),
	U3CFlickerU3Ed__3_tDBCB5374078529EF8B3F7BC65291F8A27D0EADE7::get_offset_of_U3CU3E2__current_1(),
	U3CFlickerU3Ed__3_tDBCB5374078529EF8B3F7BC65291F8A27D0EADE7::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973), -1, sizeof(CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2952[10] = 
{
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973_StaticFields::get_offset_of_instance_4(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_objectsToPreload_5(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_objectsToPreloadTimes_6(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_hideObjectsInHierarchy_7(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_spawnAsChildren_8(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_onlyGetInactiveObjects_9(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_instantiateIfNeeded_10(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_allObjectsLoaded_11(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_instantiatedObjects_12(),
	CFX_SpawnSystem_t47EAF81F8C4835A7BE978A842DE5AF5C1CF4B973::get_offset_of_poolCursors_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[15] = 
{
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_mainCamera_4(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_cameraTrs_5(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_rotSpeed_6(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_effectObj_7(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_effectObProj_8(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_arrayNo_9(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_nowEffectObj_10(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_cameraState_11(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_cameraRotCon_12(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_num_13(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_numBck_14(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_initPos_15(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_haveProFlg_16(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_nonProFX_17(),
	ConGUI_tE1D43FBC45FE6FFA2E9A701059075BF0B6D90E84::get_offset_of_tmpPos_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[15] = 
{
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_lighting_4(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_lightPower_5(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_flashFlg_6(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_flashTimer_7(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_lightKeepFlg_8(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_revOnTime_9(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_keepOnTime_10(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_keepTime_11(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_flashingFlg_12(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_minLight_13(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_maxLight_14(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_lightOffFlg_15(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_flashingOff_16(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_flashingOffPower_17(),
	Lighting_t10410C3F06ACA8870616EF0F564C4FE15E677C37::get_offset_of_flashingOffIntensity_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (U3CflashU3Ed__17_t8698C2DE91EE13B4795055166170A1AB56F976BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[3] = 
{
	U3CflashU3Ed__17_t8698C2DE91EE13B4795055166170A1AB56F976BC::get_offset_of_U3CU3E1__state_0(),
	U3CflashU3Ed__17_t8698C2DE91EE13B4795055166170A1AB56F976BC::get_offset_of_U3CU3E2__current_1(),
	U3CflashU3Ed__17_t8698C2DE91EE13B4795055166170A1AB56F976BC::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (U3CsetRevU3Ed__18_tCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[3] = 
{
	U3CsetRevU3Ed__18_tCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C::get_offset_of_U3CU3E1__state_0(),
	U3CsetRevU3Ed__18_tCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C::get_offset_of_U3CU3E2__current_1(),
	U3CsetRevU3Ed__18_tCC4F3B858DE538D9A0B855EE11687F3DD6EEF65C::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (U3CkeepOnU3Ed__19_tBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[3] = 
{
	U3CkeepOnU3Ed__19_tBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA::get_offset_of_U3CU3E1__state_0(),
	U3CkeepOnU3Ed__19_tBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA::get_offset_of_U3CU3E2__current_1(),
	U3CkeepOnU3Ed__19_tBE3860F52401D3ABF69892D9A102A0CAB4BFD0BA::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2958 = { sizeof (U3CsetFlashingOffU3Ed__20_tAE42F6A48A1229ACEC0C900F99D937D4F1793577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2958[3] = 
{
	U3CsetFlashingOffU3Ed__20_tAE42F6A48A1229ACEC0C900F99D937D4F1793577::get_offset_of_U3CU3E1__state_0(),
	U3CsetFlashingOffU3Ed__20_tAE42F6A48A1229ACEC0C900F99D937D4F1793577::get_offset_of_U3CU3E2__current_1(),
	U3CsetFlashingOffU3Ed__20_tAE42F6A48A1229ACEC0C900F99D937D4F1793577::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2959 = { sizeof (AnimateOnTrack_tC38FAC3212614A222F7ED44E3A59F28CCD6F12D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2959[3] = 
{
	AnimateOnTrack_tC38FAC3212614A222F7ED44E3A59F28CCD6F12D4::get_offset_of_trackableObject_4(),
	AnimateOnTrack_tC38FAC3212614A222F7ED44E3A59F28CCD6F12D4::get_offset_of_mTrackableBehaviour_5(),
	AnimateOnTrack_tC38FAC3212614A222F7ED44E3A59F28CCD6F12D4::get_offset_of_anim_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2960 = { sizeof (BackExit_t4B9D835283F8D0BFCF046C8CD8E88AED2DAA1871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2961 = { sizeof (FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2961[7] = 
{
	FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8::get_offset_of_originalY_4(),
	FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8::get_offset_of_rotateEnabled_5(),
	FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8::get_offset_of_rotateSpeedX_6(),
	FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8::get_offset_of_rotateSpeedY_7(),
	FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8::get_offset_of_rotateSpeedZ_8(),
	FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8::get_offset_of_floatEnabled_9(),
	FloatRotateObject_tFD65C41F499AC9B0AE4C46ABC914D2957297A3F8::get_offset_of_floatStrength_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2962 = { sizeof (HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2962[4] = 
{
	HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89::get_offset_of_ShowObectsWhenTracked_4(),
	HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89::get_offset_of_HideObectsWhenTracked_5(),
	HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89::get_offset_of_toggleStates_6(),
	HideShowOnTrack_t93975C6D0DA6560337555A88882A050AD5168D89::get_offset_of_mTrackableBehaviour_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2963 = { sizeof (PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2963[4] = 
{
	PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1::get_offset_of_audioClips_4(),
	PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1::get_offset_of_audiosource_5(),
	PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1::get_offset_of_currClip_6(),
	PlayAudioClips_t1FCE8227532CD9A81E6383B497AAE71E80F98CE1::get_offset_of_mTrackableBehaviour_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2964 = { sizeof (U3CWaitForSoundU3Ed__7_t5273E7DF11061419F414D0B1CBECDC04702B258A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2964[3] = 
{
	U3CWaitForSoundU3Ed__7_t5273E7DF11061419F414D0B1CBECDC04702B258A::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForSoundU3Ed__7_t5273E7DF11061419F414D0B1CBECDC04702B258A::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForSoundU3Ed__7_t5273E7DF11061419F414D0B1CBECDC04702B258A::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2965 = { sizeof (PlaySoundOnTrack_tEAAE33AC01707BB5D53471806DD35217226DB285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2965[3] = 
{
	PlaySoundOnTrack_tEAAE33AC01707BB5D53471806DD35217226DB285::get_offset_of_imageTarget_4(),
	PlaySoundOnTrack_tEAAE33AC01707BB5D53471806DD35217226DB285::get_offset_of_audio_5(),
	PlaySoundOnTrack_tEAAE33AC01707BB5D53471806DD35217226DB285::get_offset_of_mTrackableBehaviour_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2966 = { sizeof (StartParticlesOnTracking_t83CB5FC0A4988B440DAA2430485F059135EA98CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2966[3] = 
{
	StartParticlesOnTracking_t83CB5FC0A4988B440DAA2430485F059135EA98CC::get_offset_of_trackableObject_4(),
	StartParticlesOnTracking_t83CB5FC0A4988B440DAA2430485F059135EA98CC::get_offset_of_mTrackableBehaviour_5(),
	StartParticlesOnTracking_t83CB5FC0A4988B440DAA2430485F059135EA98CC::get_offset_of_particles_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2967 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
